<div class="wrap">
  <h1>Listado de Buscadores</h1>
    <table class="cpt-table">
      <tr>
        <th>Nombre</th>
        <th>Tipo de Buscador</th>
        <th>Shorcode</th>
        <th class="text-center">Acciones</th>
      </tr>
      <?php foreach ($options as $option) { ?>
      <tr>
        <td><?php echo $option['nombre_buscador'] ?></td>
        <td><?php echo $option['nombre_buscador'] ?></td>
        <td><?php echo $option['nombre_buscador'] ?></td>
        <td class="text-center">
         <form method="post" action="admin.php?page=fb-add-new" class="inline-block">
           <input type="hidden" name="edit_post" value="<?php echo $option['options_id'] ?>">
           <?php submit_button( 'Editar', 'primary small', 'submit', false); ?>
         </form>
         <form method="post" action="options.php" class="inline-block">
           <?php settings_fields( 'fb-new-searcher' ); ?>
           <input type="hidden" name="remove" value=<?php echo $option['options_id'] ?>>
           <?php submit_button( 'Eliminar', 'delete small', 'submit', false, array(
           'onclick' => 'return confirm("¿Estás seguro de que quieres borrar este buscador?");'
           )); ?>
         </form>
        </td>
      </tr>
      <?php } ?>
    </table>
</div>
