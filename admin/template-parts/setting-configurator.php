<div class="wrap">
  <h1>Form busqueda</h1>
  <?php settings_errors(); ?>
  <form method="post" action="options.php">
    <?php settings_fields('fb-settings-group'); ?>
    <table class="form-table">
      <tr valign="top">
        <th scope="row">Precio minimo de busqueda</th>
        <td><input type="number" name="<?php echo $this->option_name?>[<?php echo FIELD_VEHICULOS_PRECIO_MINIMO ?>]" value="<?php echo $options[FIELD_VEHICULOS_PRECIO_MINIMO]; ?>"/>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Vehiculos por página</th>
        <td><input type="number" name="<?php echo $this->option_name?>[<?php echo FIELD_VEHICULOS_POR_PAGINA ?>]" value="<?php echo $options[FIELD_VEHICULOS_POR_PAGINA]; ?>"/>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">Equipamiento de serie</th>
        <td><input type="number" name="<?php echo $this->option_name?>[<?php echo FIELD_EQUIPAMIENTO_SERIE ?>]" value="<?php echo $options[FIELD_EQUIPAMIENTO_SERIE]; ?>"/></td>
      </tr>
    </table>
    <p class="submit">
      <?php submit_button( '', 'primary', 'fb-settings-guardar', false);
      echo ' ';
      submit_button(__('Restablecer valores predeterminados'), 'secondary', 'fb-settings-reset', false); ?>
    </p>
  </form>
</div>
