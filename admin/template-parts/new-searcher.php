<ul class="nav nav-tabs">
  <li class="active"><a href="#tab-1">Buscador completo</a></li>
  <li><a href="#tab-2">Filtros Inicio</a></li>
  <li><a href="#tab-3">Filtros</a></li>
  <li><a href="#tab-4">Filtros V.Movil</a></li>
</ul>
<!-- Este es el buscador completo -->
<div class="tab-content">
   <div id="tab-1" class="tab-pane active">
      <div class="wrap">
         <h1>Buscador completo</h1>
         <?php settings_errors(); ?>
      <form method="post" action="options.php">
      <?php settings_fields('fb-new-searcher'); ?>
          <table class="form-table">
            <tr valign="top">
              <th scope="row">Introducir el nombre del buscador</th>
                <td>
                  <input type="text" name="<?php echo $this->option_new?>[nombre_buscador]" value="" required="required">
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Qué tipo de vehículo estás buscando?</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Nuevos</option>
                    <option>Nuevos en oferta</option>
                    <option>Ocasión</option>
                    <option>Ocasión + KM0</option>
                    <option>Ocasión en oferta</option>
                    <option>Ocasión (sin furgones)</option>
                    <option>Ocasión (furgones)</option>
                    <option>Subasta</option>
                    <option>KM0 no subasta</option>
                    <option>Ocasión no subasta</option> 
                    <option>Demostración</option>
                 </select>           
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Deseas mostrar los sliders?</th>
                <td>
                  <input type="checkbox" name="mostrar_slider" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Deseas ocultar alguno?</th>
                <td> 
                  <input type="checkbox" name="ocultar_precio" value=""/>Precio
                  <input type="checkbox" name="ocultar_km" value=""/>Kilómetro
                  <input type="checkbox" name="ocultar_año" value=""/>Año
                </td>
            </tr>


            <tr valign="top">
              <th scope="row">¿Cuantos vehiculos quieres mostrar en total?</th>
                <td>
                  <input type="number" name="num_vehiculos" value=""/>
                  <label for="mostrar_todos">¿Ver todos?</label>
                    <input type="checkbox" name="mostrar_todos" id="mostrar_todos" value="">
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">Ordenar por</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Marca</option>
                    <option>Modelo</option>
                    <option>Precio ascendente</option>
                    <option>Precio descendente</option>
                    <option>Aleatorio</option>
                 </select>   
                </td>
            </tr>
            
            <tr valign="top">
              <th scope="row">¿Quíeres que aparezcan los vehículos que no tengan imágenes disponibles?</th>
                <td>
                  <input type="checkbox" name="imagen_disp_vehiculo" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quiéres mostrar la tipología de los vehículos?</th>
                <td>
                  <input type="checkbox" name="mostrar_tipologia" value=""/>
                </td>  
            </tr>

            <tr valign="top">
              <th scope="row">¿Mostrar paginación?</th>
                <td>
                  <input type="checkbox" name="mostrar_paginacion" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quieres añadir una clase css?</th>
                <td>
                  <input type="text" name="clase_css" value="col-xs-12 col-sm-6 col-md-6 col-lg-4 col-xl-3"/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">Slug</th>
                <td> 
                  <input type="text" name="url_slug" value=""/>
                </td>
            </tr>
                                               
          </table>
          <p class="submit">
            <?php submit_button( 'Insertar', 'primary', 'fb-settings-guardar', false); ?>
          </p>
        </form>
      </div>

   </div>
<!-- Final del buscador completo -->
   <div id="tab-2" class="tab-pane">
      <div class="wrap">
        <h1>Filtros Inicio</h1>
        <form method="post" action="options.php">
          <?php settings_fields('fb-settings-group'); ?>
          <table class="form-table">
            <tr valign="top">
              <th scope="row">Introducir el nombre del buscador</th>
                <td>
                  <input type="text" name="nombre_buscador" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Qué tipo de vehículo estás buscando?</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Nuevos</option>
                    <option>Nuevos en oferta</option>
                    <option>Ocasión</option>
                    <option>Ocasión + KM0</option>
                    <option>Ocasión en oferta</option>
                    <option>Ocasión (sin furgones)</option>
                    <option>Ocasión (furgones)</option>
                    <option>Subasta</option>
                    <option>KM0 no subasta</option>
                    <option>Ocasión no subasta</option>
                    <option>Demostración</option>
                 </select>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Buscas un marca en concreto?</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Audi</option>
                    <option>BMW</option>
                    <option>Citröen</option>
                    <option>DS</option>
                    <option>Fiat</option>
                    <option>Land Rover</option>
                    <option>Mazda</option>
                    <option>Nissan</option>
                    <option>Opel</option>
                    <option>Peugeot</option>
                    <option>Renault</option>
                    <option>Skoda</option>
                    <option>Toyota</option>
                 </select>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">Ordenar por</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Marca</option>
                    <option>Modelo</option>
                    <option>Precio ascendente</option>
                    <option>Precio descendente</option>
                    <option>Aleatorio</option>
                 </select>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Deseas mostrar los sliders?</th>
                <td>
                  <input type="checkbox" name="mostrar_slider" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Deseas ocultar alguna?</th>
                <td>
                  <input type="checkbox" name="ocultar_precio" value=""/>Precio
                  <input type="checkbox" name="ocultar_km" value=""/>Kilómetro
                  <input type="checkbox" name="ocultar_año" value=""/>Año
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quiéres mostrar la tipología de los vehículos?</th>
                <td>
                  <input type="checkbox" name="mostrar_tipologia" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quíeres que aparezcan los vehículos que no tengan imágenes disponibles?</th>
                <td>
                  <input type="checkbox" name="imagen_disp_vehiculo" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quieres añadir una clase css?</th>
                <td>
                  <input type="text" name="clase_css" value="Predeterminada"/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Qué fichero quieres que procese los campos introducidos?</th>
                <td>
                  <input type="action" name="action" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">Template</th>
                <td>
                  <input type="url" name="template" value="/vehiculos-search.php"/>
                </td>
            </tr>

          </table>
          <p class="submit">
            <?php submit_button( 'Insertar', 'primary', 'fb-settings-guardar', false); ?>
          </p>
        </form>
      </div>
   </div>
   <div id="tab-3" class="tab-pane">
      <div class="wrap">
        <h1>Filtros</h1>
        <form method="post" action="options.php">
          <?php settings_fields('fb-settings-group'); ?>
          <table class="form-table">
            <tr valign="top">
              <th scope="row">Introducir el nombre del buscador</th>
                <td>
                  <input type="text" name="nombre_buscador" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Qué tipo de vehículo estás buscando?</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Nuevos</option>
                    <option>Nuevos en oferta</option>
                    <option>Ocasión</option>
                    <option>Ocasión + KM0</option>
                    <option>Ocasión en oferta</option>
                    <option>Ocasión (sin furgones)</option>
                    <option>Ocasión (furgones)</option>
                    <option>Subasta</option>
                    <option>KM0 no subasta</option>
                    <option>Ocasión no subasta</option>
                    <option>Demostración</option>
                 </select>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Buscas un marca en concreto?</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Audi</option>
                    <option>BMW</option>
                    <option>Citröen</option>
                    <option>DS</option>
                    <option>Fiat</option>
                    <option>Land Rover</option>
                    <option>Mazda</option>
                    <option>Nissan</option>
                    <option>Opel</option>
                    <option>Peugeot</option>
                    <option>Renault</option>
                    <option>Skoda</option>
                    <option>Toyota</option>
                 </select>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">Ordenar por</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Marca</option>
                    <option>Modelo</option>
                    <option>Precio ascendente</option>
                    <option>Precio descendente</option>
                    <option>Aleatorio</option>
                 </select>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Deseas mostrar los sliders?</th>
                <td>
                  <input type="checkbox" name="mostrar_slider" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Deseas ocultar alguna?</th>
                <td>
                  <input type="checkbox" name="ocultar_precio" value=""/>Precio
                  <input type="checkbox" name="ocultar_km" value=""/>Kilómetro
                  <input type="checkbox" name="ocultar_año" value=""/>Año
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quiéres mostrar la tipología de los vehículos?</th>
                <td>
                  <input type="checkbox" name="mostrar_tipologia" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quíeres que aparezcan los vehículos que no tengan imágenes disponibles?</th>
                <td>
                  <input type="checkbox" name="imagen_disp_vehiculo" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quieres añadir una clase css?</th>
                <td>
                  <input type="text" name="clase_css" value="Predeterminada"/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">Template</th>
                <td>
                  <input type="url" name="template" value="/vehiculos-filter.php"/>
                </td>
            </tr>

          </table>
          <p class="submit">
            <?php submit_button( 'Insertar', 'primary', 'fb-settings-guardar', false); ?>
          </p>
        </form>
      </div>
   </div>
   <div id="tab-4" class="tab-pane">
      <div class="wrap">
        <h1>Filtros V.Movil</h1>
        <form method="post" action="options.php">
          <?php settings_fields('fb-settings-group'); ?>
          <table class="form-table">
            <tr valign="top">
              <th scope="row">Introducir el nombre del buscador</th>
                <td>
                  <input type="text" name="nombre_buscador" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Qué tipo de vehículo estás buscando?</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Nuevos</option>
                    <option>Nuevos en oferta</option>
                    <option>Ocasión</option>
                    <option>Ocasión + KM0</option>
                    <option>Ocasión en oferta</option>
                    <option>Ocasión (sin furgones)</option>
                    <option>Ocasión (furgones)</option>
                    <option>Subasta</option>
                    <option>KM0 no subasta</option>
                    <option>Ocasión no subasta</option>
                    <option>Demostración</option>
                 </select>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Buscas un marca en concreto?</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Audi</option>
                    <option>BMW</option>
                    <option>Citröen</option>
                    <option>DS</option>
                    <option>Fiat</option>
                    <option>Land Rover</option>
                    <option>Mazda</option>
                    <option>Nissan</option>
                    <option>Opel</option>
                    <option>Peugeot</option>
                    <option>Renault</option>
                    <option>Skoda</option>
                    <option>Toyota</option>
                 </select>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">Ordenar por</th>
                <td>
                 <select name="tipo_vehiculo">
                    <option>Marca</option>
                    <option>Modelo</option>
                    <option>Precio ascendente</option>
                    <option>Precio descendente</option>
                    <option>Aleatorio</option>
                 </select>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Deseas mostrar los sliders?</th>
                <td>
                  <input type="checkbox" name="mostrar_slider" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Deseas ocultar alguna?</th>
                <td>
                  <input type="checkbox" name="ocultar_precio" value=""/>Precio
                  <input type="checkbox" name="ocultar_km" value=""/>Kilómetro
                  <input type="checkbox" name="ocultar_año" value=""/>Año
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quiéres mostrar la tipología de los vehículos?</th>
                <td>
                  <input type="checkbox" name="mostrar_tipologia" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quíeres que aparezcan los vehículos que no tengan imágenes disponibles?</th>
                <td>
                  <input type="checkbox" name="imagen_disp_vehiculo" value=""/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">¿Quieres añadir una clase css?</th>
                <td>
                  <input type="text" name="clase_css" value="Predeterminada"/>
                </td>
            </tr>

            <tr valign="top">
              <th scope="row">Template</th>
                <td>
                  <input type="url" name="template" value="/vehiculos-filter-movil.php"/>
                </td>
            </tr>

          </table>
          <p class="submit">
            <?php submit_button( 'Insertar', 'primary', 'fb-settings-guardar', false); ?>
          </p>
        </form>
      </div>

   </div>
</div>
<!-- Final del buscador dividido -->

