<?php
/**
 * Created by IntelliJ IDEA.
 * User: Luis
 * Date: 14/10/2019
 * Time: 10:17
 */
require_once( explode( "wp-content" , __FILE__ )[0] . "wp-load.php" );


class admin
{
  protected $option_name = OPCIONES_CONFIGURACION;
  protected $option_new = BUSCADOR_NUEVO;

  // Valores por defecto
  protected $default = array(
    FIELD_VEHICULOS_PRECIO_MINIMO => VEHICULOS_PRECIO_MINIMO_DEFAULT,
    FIELD_VEHICULOS_POR_PAGINA => VEHICULOS_POR_PAGINA_DEFAULT,
    FIELD_EQUIPAMIENTO_SERIE => EQUIPAMIENTO_SERIE_DEFAULT
  );

  public function __construct()
  {
    add_action( 'admin_menu', array( $this, 'menu_options' ) );
    add_action( 'admin_init', array( $this,'register_settings' ) );
    register_activation_hook( WP_PLUGIN_DIR . '/' . PLUGIN_NAME . '/form-busqueda.php', array ( $this , 'activar' ));
    register_deactivation_hook( WP_PLUGIN_DIR . '/' . PLUGIN_NAME . '/form-busqueda.php', array ( $this , 'deactivate' ));
    /* Suspendido por ahora
    if ( ! empty( get_option( $this->option_new ) ) ) {
      add_action( 'init', array( $this, 'registerCustomPostTypes' ) );
    }*/

    add_action( 'admin_enqueue_scripts', array( $this,'admin_add_editor_styles') );
  }

  public function menu_options()
  {
    add_menu_page(
      'Form busqueda', // Titulo de la página
      'Form busqueda', // Titulo en el menú
      'manage_options', // Permisos que debe tener un usuario para entrar en la página de configuración
      'fb-menu', // url de destino para la plantilla de edicion
      array( $this, 'create_admin_page' ), // funcion a la que redirige
      'dashicons-search' // icono
    );
    add_submenu_page(
      'fb-menu', // llamada al menu principal apartado url
      'Nuevo buscador', // Titulo de la subpágina
      'Añadir nuevo', // Titulo en el menú
      'manage_options', // permisos que debe tener un usuario para entrar en la página de configuración
      'fb-add-new', // url de destino para la plantilla de edicion
      array( $this, 'create_edit_page' )
    );
    add_submenu_page(
      'fb-menu', // llamada al menu principal apartado url
      'Configuración General', // Titulo de la subpágina
      'Configuración', // Titulo en el menú
      'manage_options', // permisos que debe tener un usuario para entrar en la página de configuración
      'fb-setting', // url de destino para la plantilla de edicion
      array( $this, 'create_setting_page' )
    );
  }

   public function register_settings()
  {
    register_setting(
      'fb-settings-group',   // Nombre del grupo de opciones de configuración
      $this->option_name,       // Nombre de opciones
      array($this, 'recursive_sanitize_text_field')   // Funcion para validar los campos
    );
    register_setting(
      'fb-new-searcher',   // Nombre del grupo de opciones de buscador nuevo
      $this->option_new,       // Nombre de opciones
      array($this, 'multidimensional_sanitize_text_field')   // Funcion para validar los campos
    );
  }

  function recursive_sanitize_text_field($array) {
    if ( isset( $_POST['fb-settings-guardar'] ) ) {
      foreach ($array as $key => $value) {
        if (is_array($value)) {
          $value = recursive_sanitize_text_field($value);
        } else {
          $value = sanitize_text_field($value);
        }
      }
    }elseif ( isset( $_POST['fb-settings-reset'] ) ) {
      $array = $this->default;
      add_settings_error( 'fb-settings-reset', 'fb-settings-reset', __( 'Lo valores predeterminados se han restaurado correctamente.', 'fb-settings' ), 'updated' );
    }
    return $array;
  }

  public function multidimensional_sanitize_text_field( $input )
  {
    $output = get_option( $this->option_new );
    $singular = replaceAcentoGuion($input['nombre_buscador']);
    $input['options_id'] = $singular;

    if(isset($_POST['remove'])){
      unset($output[$_POST['remove']]);

      return $output;
    }

    if ( count($output) == 0 ) {
      $output[$singular] = $input;

      return $output;
    }

    foreach ($output as $key => $value) {
      if ($singular === $key) {
        $output[$key] = $input;
      } else {
        $output[$singular] = $input;
      }
    }
    return $output;
  }

  public function activar() {
    update_option($this->option_name, $this->default);
    update_option($this->option_new, array());
  }

  public function deactivate() {
    delete_option($this->option_name);
  }

  public function create_admin_page()
  {
    // Obtener nombre de opciones
    $options = get_option( $this->option_new ) ?: array();

    require_once __DIR__ . '/template-parts/index-list.php';
  }
  public function create_edit_page()
  {
    // Obtener nombre de opciones
    $options = get_option( $this->option_new );

    require_once __DIR__ . '/template-parts/new-searcher.php';
  }
  public function create_setting_page()
  {
    // Obtener nombre de opciones
    $options = get_option( $this->option_name );

    require_once __DIR__ . '/template-parts/setting-configurator.php';
  }

  public function registerCustomPostTypes()
  {
    $options = get_option( $this->option_new );
    $singular = str_replace( ' ', '-', $options['nombre_buscador'] );
      register_post_type( 'form-busqueda',
      array(
        'labels' => array(
          'name' => $options['nombre_buscador'],
          'singular_name' => $singular,
        ),
        'rewrite' => false,
        'query_var' => false,
        'public' => false,
        'capability_type' => 'page',
        'capabilities' => array(
          'edit_post' => 'edit-' . $singular,
          'read_post' => 'read-' . $singular,
          'delete_post' => 'delete-' . $singular,
          'edit_posts' => 'edits-' . $singular,
          'edit_others_posts' => 'others-' . $singular,
          'publish_posts' => 'publish-' . $singular,
          'read_private_posts' => 'read-' . $singular,
        ),
      ) );
  }
  function admin_add_editor_styles()
  {
    // Menu Plugin
    wp_enqueue_style( 'mypluginstyle', plugin_dir_url( dirname( __FILE__, 1 ) ) . 'css/mypluginstyle.css' );
    wp_enqueue_script( 'mypluginscript', plugin_dir_url( dirname( __FILE__, 1 ) ) . 'js/mypluginscript.js' );
  }
}

if( is_admin() ) {
  $admin = new admin();}