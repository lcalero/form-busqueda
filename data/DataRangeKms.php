<?php

/**
 * Class DataRangeKms
 * Consulta en BBDD rango min y max de kms.
 */

class DataRangeKms {

	private $mysqli;


	/**
	 * DataRangeKms constructor.
	 *
	 * @param $mysqli
	 */
	public function __construct( $mysqli ) {
		$this->mysqli = $mysqli;
	}


	/**
	 * Devuelve min max de kms en BBDD.
	 * @return array [min,max]
	 */
	public function getValues( $params ) {
		$min = 0;
		$max = 0;

		// según vehículo.
		switch ( $params['t'] ) {

			case TIPO_VEHICULO_OCASION:
				$sqlPlus = " WHERE vn_vo = '1' AND km0 = 0";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sqlPlus .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_MAS_KM0:
				$sqlPlus = " WHERE vn_vo = '1'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sqlPlus .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_NO_FURGON:
				$sqlPlus = " WHERE vn_vo = '1' AND km0 <> 2 AND automatico=1";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sqlPlus .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_FURGON:
				$sqlPlus = " WHERE automatico IS NULL AND km0 <> 2";
				break;

			case TIPO_VEHICULO_NUEVO:
				$sqlPlus = " WHERE vn_vo = '0'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sqlPlus .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_OFERTA:
				$sqlPlus = " WHERE vn_vo = '1'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sqlPlus .= " AND pvpOfertaParticulares > 0";
				}
				break;

			case TIPO_VEHICULO_NUEVO_OFERTA:
				$sqlPlus = " WHERE vn_vo = '0'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sqlPlus .= " AND pvpOfertaParticulares > 0";
				}
				break;

            case TIPO_VEHICULO_DEMO:
                $sqlPlus = " WHERE km0 >= 1";
                if (PVP_OFERTA_PARITCULARES) {
                  $sqlPlus .= " AND pvpOfertaParticulares = 0";
                }
            break;

            case TIPO_VEHICULO_SUBASTA:
                $sqlPlus = " WHERE vn_vo = '1' and subasta = 1";
            break;

            case TIPO_VEHICULO_OCASION_NO_SUBASTA:
                $sqlPlus = " WHERE vn_vo = '1' and subasta IS NULL";
            break;

            case TIPO_VEHICULO_KM0_NO_SUBASTA:
                $sqlPlus = " WHERE vn_vo = '1' and km0 = 1";
            break;

            case TIPO_VEHICULO_ECO:
                $sqlPlus = " WHERE etiquetado = 'eco' or etiquetado = '0'";
            break;

            case TIPO_VEHICULO_KM0:
                $sqlPlus = " WHERE km0 = 1";
            break;

			default:
				$sqlPlus = " WHERE km0 >= 0";
				break;
		}

        // marca.
        $marca_shortcode = $params['marca_shortcode'];
        if ( ! empty( $marca_shortcode ) ) {
          $sqlPlus .= " AND maknatcode = '$marca_shortcode'";
        }

		// Valor mínimo.
		$sql = " SELECT MIN(kms) AS minKm" .
		       " FROM " . DB_PREFIJO . "vehiculos" .
		       " LEFT OUTER JOIN " . DB_PREFIJO . "vehiculoDatosEconomicos" .
		       " ON " . DB_PREFIJO . "vehiculos.id_vehiculos=" . DB_PREFIJO . "vehiculoDatosEconomicos.id_vehiculo";
		$sql .= $sqlPlus;

		if ( $result = $this->mysqli->query( $sql ) ) {
			if ( $row = $result->fetch_object() ) {
				$min = $row->minKm;
			}
		}

		// Valor máximo.
		$sql = " SELECT MAX(kms) AS maxKm" .
		       " FROM " . DB_PREFIJO . "vehiculos" .
		       " LEFT OUTER JOIN " . DB_PREFIJO . "vehiculoDatosEconomicos" .
		       " ON " . DB_PREFIJO . "vehiculos.id_vehiculos=" . DB_PREFIJO . "vehiculoDatosEconomicos.id_vehiculo";
		$sql .= $sqlPlus;

		if ( $result = $this->mysqli->query( $sql ) ) {
			if ( $row = $result->fetch_object() ) {
				$max = $row->maxKm;
			}
		}

		$values = [ $min, $max ];

		return $values;
	}

}