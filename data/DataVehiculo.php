<?php


class DataVehiculo {

	private $id;


	public function __construct( $id ) {
		$this->id = $id;
	}


	public function get() {
		$vehiculo = null;
		$mysqli   = conectionDB();

		$sql =  " SELECT *, concat(yearMatriculacion,lpad(mesMatriculacion,2,'0'),lpad(diaMatriculacion,2,'0')) as fechatotal, " .
            " (CASE WHEN vn_vo=0 THEN '".FINANCIACION_COD_PRODUCTO_VN."'".
            " WHEN (DATEDIFF(CURDATE(), concat(yearMatriculacion,'-',lpad(mesMatriculacion,2,'0'),'-',lpad(diaMatriculacion,2,'0')))/365)<5 THEN '".FINANCIACION_COD_PRODUCTO_VO_MENOR_5_ANNOS."'" .
            " ELSE '".FINANCIACION_COD_PRODUCTO_VO_MAYOR_5_ANNOS."' END) AS codProducto";

    $sql .= " FROM " . DB_PREFIJO . "vehiculos" .
		        " LEFT OUTER JOIN " . DB_PREFIJO . "imagenes" .
		        " ON " . DB_PREFIJO . "vehiculos.id_vehiculos=" . DB_PREFIJO . "imagenes.id_vehiculo" .
		        " LEFT OUTER JOIN " . DB_PREFIJO . "vehiculoDatosEconomicos" .
		        " ON " . DB_PREFIJO . "vehiculos.id_vehiculos=" . DB_PREFIJO . "vehiculoDatosEconomicos.id_vehiculo";

		if (EXIST_TABLE_VEHICULO_UBICACION) {
			$sql .= " LEFT OUTER JOIN " . DB_PREFIJO . "vehiculoUbicacion" .
			        " ON " . DB_PREFIJO . "vehiculos.id_ubicacion=" . DB_PREFIJO . "vehiculoUbicacion.id_ubicacion";
		}

		if (EXIST_TABLE_VEHICULO_MARCAS) {
			$sql .= " LEFT OUTER JOIN " . DB_PREFIJO . "vehiculoMarcas" .
			        " ON " . DB_PREFIJO . "vehiculoMarcas.marca=" . DB_PREFIJO . "vehiculos.maknatcode ";
		}

		$sql .= " WHERE id_vehiculos=" . $this->id;

		$result = $mysqli->query( $sql );
		if ( ! $result ) {
			throw new Exception( $mysqli->error );
		}

		$vehiculo                 = mysqli_fetch_assoc( $result );
		$vehiculo['precio']       = numberFormat( $vehiculo["pvpParticulares"] );
		$vehiculo['cv']           = floatCV( $vehiculo["typhp"] );
		$vehiculo['oferta']       = numberFormat( $vehiculo["pvpOfertaParticulares"] );
        $vehiculo['precioF']       = numberFormat( $vehiculo["pvpFinanciado"] );
		$vehiculo['km']           = numberFormat( $vehiculo["kms"] );
		$mediaconsumo             = $vehiculo["tcoconstot"];
		$vehiculo['mediaconsumo'] = ( $mediaconsumo * 100 ) / 100;

		// listado de fotos
		$sql = " SELECT foto";
        if (EXIST_FIELD_TABLE_IMAGENES){
            $sql .= ", url_video";
        }
		$sql .= " FROM " . DB_PREFIJO . "imagenes" .
		        " WHERE " . DB_PREFIJO . "imagenes.id_vehiculo=" . $this->id .
		        " ORDER BY orden";

		$result = $mysqli->query( $sql );
		if ( ! $result ) {
			throw new Exception( $mysqli->error );
		}

		$fotos = array();
		$url_video = "";
		while ( $row = mysqli_fetch_assoc( $result ) ) {
		    if(!empty($row["foto"])){
              $fotos[] = $row["foto"];
            }
            if(!empty($row["url_video"])){
		      $url_video = $row["url_video"];
            }
		}
		$vehiculo['fotos'] = $fotos;
		$vehiculo['url_video'] = $url_video;

		// listado equipamiento de serie/extra
		$sql = " SELECT eqttext, tipo" .
		       " FROM " . DB_PREFIJO . "vehiculoOpcionales" .
		       " WHERE " . DB_PREFIJO . "vehiculoOpcionales.eqttext != ''" .
		       " AND " . DB_PREFIJO . "vehiculoOpcionales.id_vehiculo=" . $this->id;

		$result = $mysqli->query( $sql );
		if ( ! $result ) {
			throw new Exception( $mysqli->error );
		}

		$serie      = array();
		$opcionales = array();
		while ( $row = mysqli_fetch_assoc( $result ) ) {
			if ( $row['tipo'] == EQUIPAMIENTO_SERIE ) {
				$serie[] = $row['eqttext'];
			} else {
				$opcionales[] = $row['eqttext'];
			}
		}
		$vehiculo['serie']      = $serie;
		$vehiculo['opcionales'] = $opcionales;

		closeDB( $mysqli );

		return $vehiculo;
	}
}


