<?php
/**
 * Created by IntelliJ IDEA.
 * User: Luis
 * Date: 13/12/2019
 * Time: 12:53
 */

class DataTransmision
{
  private $mysqli;
  private $params;


  /**
   * DataColor constructor
   *
   * @param object $mysqli Conexión a BBDD
   * @param string $tipovehiculo Identificador tipo vehículo.
   */
  public function __construct( $mysqli, $params ) {
    $this->mysqli       = $mysqli;
    $this->params = $params;
  }

  /**
   * Devuelve todos los Colores.
   * @return array
   */
  public function getAll(){
    $sql = " SELECT transmision AS value" .
      " FROM " .DB_PREFIJO . "vehiculos";

    switch ( $this->params['t'] ) {

      case TIPO_VEHICULO_NUEVO:
        $sql .= " WHERE vn_vo = '0'";
        if ( PVP_OFERTA_PARITCULARES ) {
          $sql .= " AND pvpOfertaParticulares = 0";
        }
        break;

      case TIPO_VEHICULO_OCASION:
        $sql .= " WHERE vn_vo = '1' AND km0 = 0";
        if ( PVP_OFERTA_PARITCULARES ) {
          $sql .= " AND pvpOfertaParticulares = 0";
        }
        break;

      case TIPO_VEHICULO_OCASION_MAS_KM0:
        $sql .= " WHERE vn_vo = '1'";
        if ( PVP_OFERTA_PARITCULARES ) {
          $sql .= " AND pvpOfertaParticulares = 0";
        }
        break;

      case TIPO_VEHICULO_OCASION_OFERTA:
        $sql .= " WHERE vn_vo = '1'";
        if ( PVP_OFERTA_PARITCULARES ) {
          $sql .= " AND pvpOfertaParticulares > 0";
        }
        break;

      case TIPO_VEHICULO_NUEVO_OFERTA:
        $sql .= " WHERE vn_vo = '0'";
        if ( PVP_OFERTA_PARITCULARES ) {
          $sql .= " AND pvpOfertaParticulares > 0";
        }
        break;

      case TIPO_VEHICULO_OCASION_NO_FURGON:
        $sql .= " WHERE vn_vo = '1' AND km0 <> 2 AND automatico=1";
        if ( PVP_OFERTA_PARITCULARES ) {
          $sql .= " AND pvpOfertaParticulares = 0";
        }
        break;

      case TIPO_VEHICULO_OCASION_FURGON:
        $sql .= " WHERE automatico IS NULL AND km0 <> 2";
        break;

      case TIPO_VEHICULO_DEMO:
        $sql .= " WHERE km0 >= 1";
        if (PVP_OFERTA_PARITCULARES) {
          $sql .= " AND pvpOfertaParticulares = 0";
        }
        break;

      case TIPO_VEHICULO_SUBASTA:
        $sql .= " WHERE vn_vo = '1' and subasta = 1";
        break;

      case TIPO_VEHICULO_OCASION_NO_SUBASTA:
        $sql .= " WHERE vn_vo = '1' and subasta IS NULL";
        break;

      case TIPO_VEHICULO_KM0_NO_SUBASTA:
        $sql .= " WHERE vn_vo = '1' and km0 = 1";
        break;

      case TIPO_VEHICULO_ECO:
        $sql .= " WHERE etiquetado = 'eco' or etiquetado = '0'";
        break;

      case TIPO_VEHICULO_KM0:
        $sql .= " WHERE km0 = 1";
        break;

      default:
        $sql .= " WHERE km0 >= 0";
        break;
    }

    // marca.
    $marca_shortcode = $this->params['marca_shortcode'];
    if ( ! empty( $marca_shortcode ) ) {
      $sql .= " AND maknatcode = '$marca_shortcode'";
    }

    $sql .= " GROUP BY transmision";

    $result = $this->mysqli->query( $sql );
    if ( ! $result ) {
      throw new Exception( $this->mysqli->error );
    }

    return $result;
  }
}

