<?php
/**
 * Created by IntelliJ IDEA.
 * User: Huawei
 * Date: 03/06/2018
 * Time: 21:55
 */

class DataVehiculos {
	private $mysqli;

	public function __construct( $mysqli ) {
		$this->mysqli = $mysqli;
	}



	public function getBy ($params) {

		// listado de vehículos.
		$sqlList =  " SELECT *, concat(yearMatriculacion,lpad(mesMatriculacion,2,'0'),lpad(diaMatriculacion,2,'0')) as fechatotal, " .
                " (CASE WHEN vn_vo=0 THEN '".FINANCIACION_COD_PRODUCTO_VN."'".
                " WHEN (DATEDIFF(CURDATE(), concat(yearMatriculacion,'-',lpad(mesMatriculacion,2,'0'),'-',lpad(diaMatriculacion,2,'0')))/365)<5 THEN '".FINANCIACION_COD_PRODUCTO_VO_MENOR_5_ANNOS."'" .
                " ELSE '".FINANCIACION_COD_PRODUCTO_VO_MAYOR_5_ANNOS."' END) AS codProducto, concat(maknatcode, mlocode, modnatcode, yearMatriculacion, typtxtfueltypecd2, typtxttranstypecd2, color) as busqueda";



    $sql = " FROM " . DB_PREFIJO . "vehiculos" .
           " LEFT OUTER JOIN " . DB_PREFIJO . "imagenes" .
           " ON " . DB_PREFIJO . "vehiculos.id_vehiculos=" . DB_PREFIJO . "imagenes.id_vehiculo" .
           " AND " . DB_PREFIJO . "imagenes.orden = ( select min(orden) from " . DB_PREFIJO . "imagenes where id_vehiculo = " . DB_PREFIJO . "vehiculos.id_vehiculos ) " .
           " LEFT OUTER JOIN " . DB_PREFIJO . "vehiculoDatosEconomicos" .
           " ON " . DB_PREFIJO . "vehiculos.id_vehiculos=" . DB_PREFIJO . "vehiculoDatosEconomicos.id_vehiculo";

		if (EXIST_TABLE_VEHICULO_UBICACION) {
			$sql .= " LEFT OUTER JOIN " . DB_PREFIJO . "vehiculoUbicacion" .
			        " ON " . DB_PREFIJO . "vehiculos.id_ubicacion=" . DB_PREFIJO . "vehiculoUbicacion.id_ubicacion";
		}

		if (EXIST_TABLE_VEHICULO_MARCAS) {
			$sql .= " LEFT OUTER JOIN " . DB_PREFIJO . "vehiculoMarcas" .
			        " ON " . DB_PREFIJO . "vehiculoMarcas.marca=" . DB_PREFIJO . "vehiculos.maknatcode ";
		}

		// según vehículo.
		$tipo_vehiculos = $params['t'];
		switch ($tipo_vehiculos) {

			case TIPO_VEHICULO_OCASION:
				$sql .= " WHERE vn_vo = '1' AND km0 = 0";
				if (PVP_OFERTA_PARITCULARES) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
			break;

            case TIPO_VEHICULO_OCASION_MAS_KM0:
              $sql .= " WHERE vn_vo = '1'";
              if (PVP_OFERTA_PARITCULARES) {
                $sql .= " AND pvpOfertaParticulares = 0";
              }
            break;

			case TIPO_VEHICULO_OCASION_NO_FURGON:
				$sql .= " WHERE vn_vo = '1' AND km0 <> 2 AND automatico=1";
				if (PVP_OFERTA_PARITCULARES) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
			break;

			case TIPO_VEHICULO_OCASION_FURGON:
				$sql .= " WHERE automatico IS NULL AND km0 <> 2";
			break;

			case TIPO_VEHICULO_NUEVO:
				$sql .= " WHERE vn_vo = '0'";
				if (PVP_OFERTA_PARITCULARES) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
			break;

			case TIPO_VEHICULO_OCASION_OFERTA:
				$sql .= " WHERE vn_vo = '1'";
				if (PVP_OFERTA_PARITCULARES) {
					$sql .= " AND pvpOfertaParticulares > 0";
				}
				break;

			case TIPO_VEHICULO_NUEVO_OFERTA:
				$sql .= " WHERE vn_vo = '0'";
				if (PVP_OFERTA_PARITCULARES) {
					$sql .= " AND pvpOfertaParticulares > 0";
				}
			break;

            case TIPO_VEHICULO_DEMO:
                $sql .= " WHERE km0 >= 1";
                if (PVP_OFERTA_PARITCULARES) {
                  $sql .= " AND pvpOfertaParticulares = 0";
                }
            break;

            case TIPO_VEHICULO_SUBASTA:
                $sql .= " WHERE vn_vo = '1' and subasta = 1";
            break;

            case TIPO_VEHICULO_OCASION_NO_SUBASTA:
                $sql .= " WHERE vn_vo = '1' and subasta IS NULL";
            break;

            case TIPO_VEHICULO_KM0_NO_SUBASTA:
                $sql .= " WHERE vn_vo = '1' and km0 = 1";
            break;

            case TIPO_VEHICULO_ECO:
                $sql .= " WHERE etiquetado = 'eco' or etiquetado = '0'";
            break;

            case TIPO_VEHICULO_KM0:
                $sql .= " WHERE km0 = 1";
            break;

			default:
				$sql .= " WHERE km0 >= 0";
			break;
		}

		// que contenga imagen.
        $shownoimg = ($params['shownoimg'] === 'true') ? '' : ' AND orden is not NULL';
		$sql .= "$shownoimg";

		// marca.
		$marca_shortcode = $params['marca_shortcode'];
		if ( empty( $marca_shortcode ) ) {
			$ma = $params['ma'];
		} else {
			$ma = $marca_shortcode;
		}
		if ( ! empty( $ma ) ) {
			$sql .= " AND maknatcode = '$ma'";
		}

		// modelo.
		$mo = $params['mo'];
		if (!empty($mo)) {
			$sql .= " AND mlocode = '$mo'";
		}

		// combustible.
		$co = $params['co'];
		if (!empty($co)) {
			$sql .= " AND typtxtfueltypecd2 = '$co'";
		}

		// precio.
		$minprecio = $params['minprecio'];
		$maxprecio = $params['maxprecio'];
		if (!empty($minprecio) && !empty($maxprecio)) {
			$sql .= " AND (case WHEN pvpOfertaParticulares > 0 THEN pvpOfertaParticulares ELSE pvpParticulares END) BETWEEN $minprecio AND $maxprecio";
		}

		// kilometros.
		$minkm = $params['minkm'];
		$maxkm = $params['maxkm'];
		if (($minkm == 0 || !empty($minkm)) && !empty($maxkm)) {
			$sql .= " AND kms BETWEEN $minkm AND $maxkm";
		}

		// antiguedad.
		$minanno = $params['minanno'];
		$maxanno = $params['maxanno'];
		if (!empty($minanno) && !empty($maxanno)) {
			$sql .= " AND yearMatriculacion BETWEEN $minanno AND $maxanno";
		}

		// color
        $color = $params['color'];
        if (!empty($color)) {
          $sql .= " AND colorPrimario = '$color'";
        }

        // transmision
        $transmision = $params['trans'];
        if (!empty($transmision)) {
          $sql .= " AND transmision = '$transmision'";
        }

        // ubicación
        $ubicacion = $params['ubica'];
        if (!empty($ubicacion)) {
          $sql .= " AND nombreSede = '$ubicacion'";
        }

		// tipología.
		$tp = $params['tp'];
		if (!empty($tp)) {
			$sql .= " AND StructureDescription = '$tp'";
		}

		// agrupado por
		$sql .= " GROUP BY " . DB_PREFIJO . "vehiculos.id_vehiculos";
		$sqlList .= $sql;

/*
		$sqlTotal = $sqlList;
		$start = $params['start'];
		if (!empty($start) && $params['showpagination'] !== 'true' && $start != 'ALL') {
			$sqlTotal .= " LIMIT " . $start;
		}

		$result = $this->mysqli->query($sqlTotal);
		if (!$result) {
			throw new Exception(
				$this->mysqli->error);
		}
		else {
			$total = mysqli_num_rows($result);
		}
*/

		// ordenación.
		$orderby = $params['orderby'];
		if($orderby == 'aleatorio'){
          $orderby = 'RAND()';
        }
		// ordenación descendente.
		$orderbydesc = $params['orderbydesc'];

		if (!empty($orderby)) {
			$sqlList .= " ORDER BY " . $orderby;
		}
		else if (!empty($orderbydesc)) {
			$sqlList .= " ORDER BY " . $orderbydesc . " DESC";
		}
		else {
			$sqlList .= " ORDER BY pvpParticulares DESC";
		}


		// resultados por paginacion.
      /*
		$pag = $params['pag'];
		if (!empty($pag)) {
			$sqlList .= " LIMIT " . ( ($pag - 1) * VEHICULOS_POR_PAGINA) . "," . VEHICULOS_POR_PAGINA;
		}

		// resultados sin paginación con límite.
		else {
			if (!empty($start)) {
				if ($start != 'ALL') {
					$sqlList .= " LIMIT " . $start;
				}
			}
			else {
				$sqlList .= " LIMIT " . VEHICULOS_POR_PAGINA;
			}
		}
		*/
		// resultados.
		$result = $this->mysqli->query($sqlList);
		if (!$result) {
			throw new Exception($this->mysqli->error);
		}

		return $result;

	}


}
