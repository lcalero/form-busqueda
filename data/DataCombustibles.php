<?php

/**
 * Class DataCombustibles
 * Consulta combustibles a BBDD.
 */

class DataCombustibles {

	private $mysqli;
	private $params;


	/**
	 * DataCombustibles constructor.
	 *
	 * @param object $mysqli Conexión a BBDD.
	 * @param string $tipovehiculo Indentificador tipo vehículo.
	 */
	public function __construct( $mysqli, $params ) {
		$this->mysqli = $mysqli;
		$this->params = $params;
	}


	/**
	 * Devuelve todos Combustibles.
	 * @return array
	 */
	public function getAll() {
		return $this->execute( null, null );
	}


	/**
	 * Devuelve todos los combustibles de según marca, modelo.
	 *
	 * @param string $marca - Identificador de marca.
	 * @param string $modelo - Identificador de modelo.
	 *
	 * @return array
	 */
	public function getBy( $marca, $modelo ) {
		return $this->execute( $marca, $modelo );
	}


	/**
	 * Devuelve los combustible según los criterios, si son nulos devuelve todos.
	 *
	 * @param string $marca - Identificador de marca.
	 * @param string $modelo - Identificador de modelo.
	 *
	 * @return array
	 * @throws Exception
	 */
	private function execute( $marca, $modelo ) {
		$sql = " SELECT typtxtfueltypecd2 AS value" .
		       " FROM " . DB_PREFIJO . "vehiculos" .
		       " INNER JOIN " . DB_PREFIJO . "vehiculoDatosEconomicos" .
		       " ON " . DB_PREFIJO . "vehiculos.id_vehiculos = " . DB_PREFIJO . "vehiculoDatosEconomicos.id_vehiculo";

		$sql .= " WHERE";

		if ( ! empty( $marca ) && ! empty( $modelo ) ) {
			$sql .= " maknatcode = '" . $marca . "'" .
			        " AND mlocode = '" . $modelo . "'" .
			        " AND";
		}

		switch ( $this->params['t'] ) {

			case TIPO_VEHICULO_OCASION:
				$sql .= " vn_vo = '1' AND km0 = 0";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_MAS_KM0:
				$sql .= " vn_vo = '1'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_NO_FURGON:
				$sql .= " vn_vo = '1' AND km0 <> 2 AND automatico=1";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_FURGON:
				$sql .= " automatico IS NULL AND km0 <> 2";
				break;

			case TIPO_VEHICULO_NUEVO:
				$sql .= " vn_vo = '0'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_OFERTA:
				$sql .= " vn_vo = '1'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares > 0";
				}
				break;

			case TIPO_VEHICULO_NUEVO_OFERTA:
				$sql .= " vn_vo = '0'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares > 0";
				}
				break;

            case TIPO_VEHICULO_DEMO:
                $sql .= " km0 >= 1";
                if (PVP_OFERTA_PARITCULARES) {
                  $sql .= " AND pvpOfertaParticulares = 0";
                }
            break;

            case TIPO_VEHICULO_SUBASTA:
                $sql .= " vn_vo = '1' and subasta = 1";
            break;

            case TIPO_VEHICULO_OCASION_NO_SUBASTA:
                $sql .= " vn_vo = '1' and subasta IS NULL";
            break;

            case TIPO_VEHICULO_KM0_NO_SUBASTA:
                $sql .= " vn_vo = '1' and km0 = 1";
            break;

            case TIPO_VEHICULO_ECO:
                $sql .= " etiquetado = 'eco' or etiquetado = '0'";
            break;

            case TIPO_VEHICULO_KM0:
                $sql .= " km0 = 1";
            break;

            default:
				$sql .= " km0 >= 0";
			break;
		}

        // marca.
        $marca_shortcode = $this->params['marca_shortcode'];
        if ( ! empty( $marca_shortcode ) ) {
          $sql .= " AND maknatcode = '$marca_shortcode'";
        }

		$sql .= " GROUP BY typtxtfueltypecd2";

		$result = $this->mysqli->query( $sql );
		if ( ! $result ) {
			throw new Exception( $this->mysqli->error );
		}

		return $result;

	}


}