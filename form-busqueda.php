<?php


/**
 *
 * Plugin Name: form-busqueda
 * Plugin URL: no url
 * Description: formulario de busqueda
 * Version: 3.0.0
 * Author: Juan Carlos Muruaga y Luis Calero
 *
 * */


/**
 * Crea los combos marca, modelo y combustible
 * los sliders precio, km y antigüeda
 * contenedor de vehículos
 * para la búsqueda según tipo de vehículos.
 * @param {array} $atts - Atributos de ejecución.
 * Luis Calero
 */
function form_busqueda( $atts ) {
	$tipovehiculo = '';
	$numcolsclass = '';
	$start = null;
	$showpagination=true;
    $path_info = '';
    $shownoimg = false;
    $marca_shortcode = '';
	$showsliders = false;
	$hideprecio = false;
	$hidekm = false;
	$hideanno = false;
	$showtipologia = false;

	$orderby = '';
	$orderbydesc = '';

  // Parámetros.
  extract( shortcode_atts( array(
    'tipovehiculo' => '',
    'showsliders' => false,
    'hideprecio' => false,
    'hidekm' => false,
    'hideanno' => false,
    'showtipologia' => false,
    'marca_shortcode' =>'',
    'path_info' => '',
    'shownoimg' => false,
    'numcolsclass' => 'col-sm-4',
    'start'=>VEHICULOS_CANTIDAD_INICIAL,
		'showpagination'=>true,
		'orderby' =>'',
		'orderbydesc' =>''
  ), $atts ) );

  // Contenedor de vehículos.
  $params = [
    't'=> $tipovehiculo,
    'numcolsclass' => $numcolsclass,
    'showpagination' => $showpagination,
    'start'=>$start,
    'orderby'=>$orderby,
    'orderbydesc'=>$orderbydesc,
    'marca_shortcode'=>$marca_shortcode,
    'path_info' => $path_info,
    'shownoimg' => $shownoimg
  ];

  if (isPostRequestMethod()) {
    $params = array_merge($params, $_POST);
  }
  else {
    $params = array_merge($params, $_GET);
  }
  // Conexión BBDD.
  $mysqli = conectionDB();

  $ma = getValueOnRequest('ma');
  $mo = getValueOnRequest('mo');
  $co = getValueOnRequest('co');
  $preciovalmin = getValueOnRequest('minprecio');
  $preciovalmax = getValueOnRequest('maxprecio');
  $kmvalmin = getValueOnRequest('minkm');
  $kmvalmax = getValueOnRequest('maxkm');
  $annovalmin = getValueOnRequest('minanno');
  $annovalmax = getValueOnRequest('maxanno');
  $order = getValueOnRequest('order');

  $dataPrecios = new DataRangePrecios($mysqli);
  $valuesPrecios = $dataPrecios->getValues($params);
  $minprecio = $valuesPrecios[0];
  $maxprecio = $valuesPrecios[1];

  $dataKm = new DataRangeKms($mysqli);
  $valuesKm = $dataKm->getValues($params);
  $minkm = $valuesKm[0];
  $maxkm = $valuesKm[1];

  $dataAnnos = new DataRangeAnnos($mysqli);
  $valuesAnnos = $dataAnnos->getValues($params);
  $minanno = $valuesAnnos[0];
  $maxanno = $valuesAnnos[1];

  $formOptions = new HTMLOptions($params);
  $tipologias = new HTMLTipologias();
  $dataVehiculos = new DataVehiculos( $mysqli );
  //$data = $dataVehiculos->getBy( $params );

  if (TEMPLATE_VEHICULOS_FILTER) {
      include get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/vehiculos-filter.php';
  }
  else {
      include 'html/template-parts/vehiculos-filter.php';
  }


  printVehiculos( $mysqli, $params );

  // Fin conexión BBDD.
  closeDB($mysqli);
}


/**
 * Crea los combos marca, modelo y combustible
 * los sliders precio, km y antigüeda según la plantilla
 * y con un botón buscar que redirige a el buscador principal
 * @param {array} $atts - Atributos de ejecución.
 */
function form_busqueda_filter( $atts ) {
	form_composer( $atts , 'vehiculos-filter.php');
}


/**
 * Crea los combos marca, modelo y combustible
 * los sliders precio, km y antigüeda según la plantilla
 * y con un botón buscar que redirige a el buscador principal
 * para la vista mobile.
 * @param {array} $atts - Atributos de ejecución.
 */
function form_busqueda_filter_mobile( $atts ) {
	form_composer( $atts , 'vehiculos-filter-mobile.php');
}


/**
 * Crea los combos marca, modelo y combustible
 * los sliders precio, km y antigüeda según la plantilla
 * y con un botón buscar que redirige a el buscador principal
 * para la vista mobile.
 * @param {array} $atts - Atributos de ejecución.
 */
function form_busqueda_search_mobile( $atts ) {
	form_composer( $atts , 'vehiculos-search-mobile.php');
}

/**
 * Crea los combos marca, modelo y combustible
 * los sliders precio, km y antigüeda según la plantilla
 * y con un botón buscar que redirige a el buscador principal
 * @param {array} $atts - Atributos de ejecución.
 */
function form_composer( $atts , $defaultTemplate) {
	$tipovehiculo = '';
	$numcolsclass = '';

  $showsliders = false;
  $hideprecio = false;
  $hidekm = false;
  $hideanno = false;
  $showtipologia = false;
  $marca_shortcode = '';
	$orderby = '';
	$orderbydesc = '';
	$template = '';
  $action = '';
  $shownoimg = false;

	// Parámetros.
	extract( shortcode_atts( array(
		'tipovehiculo' => '',
		'numcolsclass' => 'col-sm-4',
		'showsliders' => false,
        'hideprecio' => false,
        'hidekm' => false,
        'hideanno' => false,
        'showtipologia' => false,
        'marca_shortcode' =>'',
		'orderby' =>'',
		'orderbydesc' =>'',
		'template' =>'',
        'action' =>'',
        'shownoimg' => false
	), $atts ) );

    // obtención de parametros para realizar la busqueda y guardar el total de vehículos
    // Así podremos mostrar el resultado en un botón
    $params = [
      't'=> $tipovehiculo,
      'start'=> 'ALL',
      'marca_shortcode'=>$marca_shortcode,
      'shownoimg' => $shownoimg
    ];

    if (isPostRequestMethod()) {
      $params = array_merge($params, $_POST);
    }
    else {
      $params = array_merge($params, $_GET);
    }

    // Conexión BBDD.
	$mysqli = conectionDB();

	$ma = getValueOnRequest('ma');
	$mo = getValueOnRequest('mo');
	$co = getValueOnRequest('co');
    $preciovalmin = getValueOnRequest('minprecio');
	$preciovalmax = getValueOnRequest('maxprecio');
    $kmvalmin = getValueOnRequest('minkm');
    $kmvalmax = getValueOnRequest('maxkm');
    $annovalmin = getValueOnRequest('minanno');
    $annovalmax = getValueOnRequest('maxanno');
	$order = getValueOnRequest('order');

    $dataPrecios = new DataRangePrecios($mysqli);
    $valuesPrecios = $dataPrecios->getValues($params);
    $minprecio = $valuesPrecios[0];
    $maxprecio = $valuesPrecios[1];

    $dataKm = new DataRangeKms($mysqli);
    $valuesKm = $dataKm->getValues($params);
    $minkm = $valuesKm[0];
    $maxkm = $valuesKm[1];
  
    $dataAnnos   = new DataRangeAnnos( $mysqli );
    $valuesAnnos = $dataAnnos->getValues( $params );
    $minanno     = $valuesAnnos[0];
    $maxanno     = $valuesAnnos[1];

    $dataVehiculos = new DataVehiculos( $mysqli );
    //$data = $dataVehiculos->getBy( $params );

    $formOptions = new HTMLOptions($params);

    $tipologias = new HTMLTipologias();


	if (!empty($template)) {
		include get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/' . $template;
	}
	else {
		include 'html/template-parts/' . $defaultTemplate;
	}

	// Fin conexión BBDD.
	closeDB($mysqli);
}



/**
 * Lista de vehícullos consultados.
 * @param {array} $atts - Atributos de ejecución.
 */
function form_busqueda_vehiculos_list( $atts ) {
	$tipovehiculo = null;
	$numcolsclass = '';
    $showpagination= true;
	$start = null;
    $marca_shortcode = '';
	$orderby = '';
	$orderbydesc = '';
    $path_info = '';
    $shownoimg = false;

	// Parámetros.
	extract( shortcode_atts( array(
		'tipovehiculo' => null,
		'numcolsclass' => 'col-sm-4',
        'showpagination'=> true,
		'start'=>VEHICULOS_CANTIDAD_INICIAL,
		'orderby' =>'',
		'orderbydesc' =>'',
        'marca_shortcode' =>'',
        'path_info' => '',
        'shownoimg' => false
	), $atts ) );

	// Conexión BBDD.
	$mysqli = conectionDB();

    $ma = getValueOnRequest('ma');
    $mo = getValueOnRequest('mo');
    $co = getValueOnRequest('co');
    $preciovalmin = getValueOnRequest('minprecio');
    $preciovalmax = getValueOnRequest('maxprecio');
    $kmvalmin = getValueOnRequest('minkm');
    $kmvalmax = getValueOnRequest('maxkm');
    $annovalmin = getValueOnRequest('minanno');
    $annovalmax = getValueOnRequest('maxanno');
    $order = getValueOnRequest('order');

    $dataPrecios = new DataRangePrecios($mysqli);
    $valuesPrecios = $dataPrecios->getValues($tipovehiculo);
    $minprecio = $valuesPrecios[0];
    $maxprecio = $valuesPrecios[1];

    $dataKm = new DataRangeKms($mysqli);
    $valuesKm = $dataKm->getValues($tipovehiculo);
    $minkm = $valuesKm[0];
    $maxkm = $valuesKm[1];

    $dataAnnos = new DataRangeAnnos($mysqli);
    $valuesAnnos = $dataAnnos->getValues($tipovehiculo);
    $minanno = $valuesAnnos[0];
    $maxanno = $valuesAnnos[1];

    $params = [
      't'=> $tipovehiculo,
      'numcolsclass' => $numcolsclass,
      'showpagination' => $showpagination,
      'start'=> $start,
      'orderby'=>$orderby,
      'orderbydesc'=>$orderbydesc,
      'marca_shortcode'=>$marca_shortcode,
      'path_info' => $path_info,
      'shownoimg' => $shownoimg
    ];

    if (isPostRequestMethod()) {
      $params = array_merge($params, $_POST);
    }
    else {
      $params = array_merge($params, $_GET);
    }

	printVehiculos( $mysqli, $params );
	// Fin conexión BBDD.
	closeDB($mysqli);
}



/**
 * Muestra los datos del vehículo
 */
function form_busqueda_vehiculo_info() {
	require_once __DIR__ . '/data/DataVehiculo.php';
	if (!empty(TEMPLATE_VEHICULO_INFO)) {
		include( get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/vehiculo-info.php');
	}
	else {
		include 'html/template-parts/vehiculo-info.php';
	}
};

/**
 * Muestra los botones de cada marca de vehiculo y cada modelo
 * @param $atts - Atributos de ejecución.
 */
function from_busqueda_vehiculo_button( $atts ){
  $tipovehiculo = null;
  $tipo = '';
  $action = '';
  // Parámetros.
  extract( shortcode_atts( array(
    'tipovehiculo' => '',
    'tipo' => 'marca',
    'action' => '/vehiculos-de-ocasion'
  ), $atts ) );

  // Conexión BBDD.
  $mysqli = conectionDB();

  $params = [
    't'=> $tipovehiculo,
    'tipo'=> $tipo,
    'action' => $action
  ];

  $marcas = new DataMarcas($mysqli, $params);
  $resultMA = $marcas->getAll();

  $resultMO = array();
  foreach ($resultMA as $marca) {
    $modelos = new DataModelos($mysqli, $params);
    $resultMO[$marca['value']] = $modelos->getBy($marca['value']);
  }
  $tipologias = new DataTipologias($mysqli, $params['t']);

  $file_path_absolute = get_theme_file_path( 'form-busqueda/template-parts/vehiculo-buttons.php' );
  if ( file_exists( $file_path_absolute ) ) {
    include $file_path_absolute;
  }else{
    include ('html/template-parts/vehiculo-buttons.php');
  }
}

/**
 * Comparador de vehiculos
 */
function form_busqueda_vehiculo_compare() {
  require_once __DIR__ . '/data/DataVehiculo.php';

  if ( file_exists( get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/vehiculo-compare.php') ) {
    include( get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/vehiculo-compare.php');
  }else{
    include 'html/template-parts/vehiculo-compare.php';
  }
};

/**
 * Muestra los datos del vehículo en reserva woocomerce
 */
function form_busqueda_vehiculo_woo() {
  require_once __DIR__ . '/data/DataVehiculo.php';
  if (!empty(TEMPLATE_VEHICULO_WOO)) {
    include( get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/vehiculo-woo.php');
  }
  else {
    include 'html/template-parts/vehiculo-woo.php';
  }
};

/**
 * Añadir al formulario de woo los valores del vehiculo
 */
function form_busqueda_add_woo(){
  $marca = $_SESSION['mar-coche'];
  $modelo = $_SESSION['mod-coche'];
  $version = $_SESSION['ver-coche'];
  $url = 'https://'.$_SERVER["HTTP_HOST"].$_SESSION['url-coche'];

  include 'html/template-parts/vehiculo-woo-script.php';
}


/**
 * @param $mysqli
 * @param $tipovehiculo
 */
function printSelects( $mysqli, $tipovehiculo ) {
	// Row contenedor.
	printf('<div class="row vehiculos">');
	// Combo Marcas.
	printMarcas( $mysqli, $tipovehiculo );
	// Combo Modelos.
	printModelos( $tipovehiculo );
	// Combo Combustibles.
	printCombustibles( $mysqli, $tipovehiculo );
	// Fin row.
	printf('</div>');
}



/**
 * @param $mysqli
 * @param $tipovehiculo
 * @param $hideprecio
 * @param $hidekm
 * @param $hideanno
 */
function printSliders( $mysqli, $tipovehiculo, $hideprecio, $hidekm , $hideanno ) {
	// Row contenedor.
	printf('<div class="form-group row selectores">');
	// Slider Precio
	if (!$hideprecio) {
		printPrecioSlider($tipovehiculo, $mysqli);
	}
	// Slider Km
	if (!$hidekm) {
		printKmSlider($tipovehiculo, $mysqli);
	}
	// Slider Anno.
	if (!$hideanno) {
		printAntiguedadSlider($tipovehiculo, $mysqli);
	}
	// Fin row.
	printf('</div>');
}


function printResetButton($tipovehiculo) {
	global $FORM_LOCALES;
	printf('<button class="reset" t-vehiculo="' . $tipovehiculo . '">' . $FORM_LOCALES['clean'] . '</button>');
}



/**
 * @param $mysqli
 * @param $params
 */
function printVehiculos( $mysqli, $params ) {
	printf('<script type="text/javascript">'.
      'var numcolsclass_' . $params['t'] . '="'. $params['numcolsclass'] . '";'.
      'var marca_shortcode="'.$params['marca_shortcode'].'";'.
      'var showpagination="'.$params['showpagination'].'";'.
      'var start="'.$params['start'].'";'.
      'var orderby="'.$params['orderby'].'"'.
    '</script>');

	// Lista de vehículos.
	printf('<div id="ContenedorVehiculos" class="container-fluid">');

	$busqueda = new HTMLVehiculos();
	$busqueda->printBy($mysqli, $params);
	printf('</div>'); // Fin row.
}



/**
 * Marcas combo.
 * @param [object] $mysqli - Instancia BBDD,
 * @param {string} $tipovehiculo - Indentificador tipo vehículo.
 */
function printMarcas( $mysqli, $tipovehiculo ) {
	global $FORM_LOCALES;
  printf('<div class="form-group col-sm-4 bottom-30 form-busqueda-part">');
  printf('<p class="text-center">' . $FORM_LOCALES['marcas']['title'] . '</p>');
  printf('<select class="form-control marca" name="ma" t-vehiculo="' . $tipovehiculo . '">');
  // Opciones marcas.
  $formOptions = new HTMLOptions($tipovehiculo);
  $formOptions->printAllMarcas( $mysqli, null);
  printf('</select>');
  printf('</div>');
}



/**
 * Modelos combo.
 * @param {string} $tipovehiculo - Indentificador tipo vehículo.
 * @param [object] $mysqli - Instancia BBDD,
 */
function printModelos( $tipovehiculo ) {
	global $FORM_LOCALES;
  printf('<div class="form-group col-sm-4 bottom-30 form-busqueda-part">');
  printf('<p class="text-center">' . $FORM_LOCALES['modelos']['title'] . '</p>');
  printf('<select class="form-control modelo" name="mo" t-vehiculo="' . $tipovehiculo . '">');
  // Opción inicial.
  printf('<option disabled="disabled" selected="selected">' . $FORM_LOCALES['modelos']['default'] . '</option>');
  printf('</select>');
  printf('</div>');
}



/**
 * Combustibles combo.
 * @param [object] $mysqli - Instancia BBDD,
 * @param {string} $tipovehiculo - Indentificador tipo vehículo.
 */
function printCombustibles( $mysqli, $tipovehiculo ) {
	global $FORM_LOCALES;
	printf('<div class="form-group col-sm-4 bottom-30 form-busqueda-part">');
  printf('<p class="text-center">' . $FORM_LOCALES['combustibles']['title'] . '</p>');
  // Opción inicial.
  printf('<select class="form-control combustible" name="co" t-vehiculo="' . $tipovehiculo . '">');
  // Opciones marcas.
  $formOptions = new HTMLOptions($tipovehiculo);
  $formOptions->printAllCombustibles($mysqli, null);
  printf('</select>');
  printf('</div>');
}



/**
 * Precios slider.
 * @param {string} $tipovehiculo - Indentificador tipo vehículo.
 * @param [object] $mysqli - Instancia BBDD,
 */
function printPrecioSlider( $tipovehiculo, $mysqli ) {
  printf('<div class="form-group col-md-4 form-busqueda-part">');
    $slider = new HTMLSliders($tipovehiculo);
    $slider->printPrecio($mysqli);
  printf('</div>');
}



/**
 * Km slider.
 * @param {string} $tipovehiculo - Indentificador tipo vehículo.
 * @param [object] $mysqli - Instancia BBDD,
 */
function printKmSlider( $tipovehiculo, $mysqli ) {
  printf('<div class="form-group col-md-4 form-busqueda-part">');
  $slider = new HTMLSliders($tipovehiculo);
  $slider->printKm($mysqli);
  printf('</div>');
}



/**
 * Año slider.
 * @param {string} $tipovehiculo - Indentificador tipo vehículo.
 * @param [object] $mysqli - Instancia BBDD,
 */
function printAntiguedadSlider( $tipovehiculo, $mysqli ) {
  printf('<div class="form-group col-md-4 form-busqueda-part">');
  $slider = new HTMLSliders($tipovehiculo);
  $slider->printAnno($mysqli);
  printf('</div>');
}



/**
 * Tipologias.
 * @param [object] $mysqli - Instancia BBDD,
 * @param {string} $tipovehiculo - Indentificador tipo vehículo.
 */
function printTipologias( $mysqli, $tipovehiculo ) {
	printf('<div class="form-group col-sm-12 form-busqueda-part">');
	$tipologias = new HTMLTipologias();
	$tipologias->printBy( $mysqli, $tipovehiculo );
	printf('</div>');
}



/**
 * CSS & JS.
 */
function enqueue_scripts() {
	// jquery
  wp_enqueue_style( 'fb-jquery-ui', plugins_url('/' . PLUGIN_NAME . '/libs/jquery-ui/css/nouislider.min.css'), __FILE__);
  wp_enqueue_script( 'fb-jquery-ui', plugins_url('/' . PLUGIN_NAME . '/libs/jquery-ui/js/nouislider.min.js'), array( 'jquery' ), '1.0.0', true  );
  wp_enqueue_script( 'fb-jquery-redirect', plugins_url('/' . PLUGIN_NAME . '/libs/jquery-redirect/jquery.redirect.js'), array( 'jquery' ), '1.0.0', true  );

  // boostrap
	wp_enqueue_style( 'fb-bootstrap', plugins_url('/' . PLUGIN_NAME . '/libs/bootstrap/css/bootstrap.min.css'), __FILE__);
	wp_enqueue_style( 'fb-bootstrap2', plugins_url('/' . PLUGIN_NAME . '/libs/bootstrap/css/bootstrap-grid.min.css'), __FILE__);

  // form busqueda
	wp_enqueue_style( 'fb-icons', plugins_url('/' . PLUGIN_NAME . '/fonts/girsanet-icons.css'), __FILE__);
	wp_enqueue_script( 'fb-script', plugins_url('/' . PLUGIN_NAME . '/js/form-busqueda.js'), array( 'jquery' ), '1.1.0', true  );


	if (CSS_VEHICULOS_LIST) {
		wp_enqueue_style('fb-style-theme', get_stylesheet_directory_uri() . '/'. PLUGIN_NAME . '/css/form-busqueda.css', __FILE__);
	}else{
    wp_enqueue_style( 'fb-style-theme', plugins_url('/' . PLUGIN_NAME . '/css/form-busqueda.css'), __FILE__);
  }

  if (CSS_VEHICULOS_INFO) {
    wp_enqueue_style('fb-style-file', get_stylesheet_directory_uri() . '/'. PLUGIN_NAME . '/css/ficha-vehiculo.css', __FILE__);
  }

	// ajaxs
  wp_localize_script('fb-script', 'form_busqueda_ajax', ['url'=>admin_url('admin-ajax.php')]);
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );



/**
 * Classes.
 */
function requires_once() {
	require_once 'form-busqueda-config.php';
	require_once 'form-busqueda-common.php';

  require_once 'lang/locales-' . PLUGIN_LANG . '.php';
  if (PLUGIN_LANG_EXTEND) {
	  require_once get_stylesheet_directory() . '/' . PLUGIN_NAME . '/lang/locales-' . PLUGIN_LANG . '.php';
  }

  require_once 'html/HTMLOptions.php';
  require_once 'html/HTMLSliders.php';
	require_once 'html/HTMLTipologias.php';
  require_once 'html/HTMLVehiculos.php';

	require_once 'ajax/options-ajax.php';
	require_once 'ajax/busqueda-ajax.php';

  //require_once 'admin/admin.php';
}
requires_once();

global $wpdb;
$table_vehiculo_marcas = DB_PREFIJO . 'vehiculoMarcas';
define('EXIST_TABLE_VEHICULO_MARCAS', ( $wpdb->get_var("SHOW TABLES LIKE '$table_vehiculo_marcas'") == $table_vehiculo_marcas));
$table_vehiculo_ubicacion = DB_PREFIJO . 'vehiculoUbicacion';
define('EXIST_TABLE_VEHICULO_UBICACION', ( $wpdb->get_var("SHOW TABLES LIKE '$table_vehiculo_ubicacion'") == $table_vehiculo_ubicacion));
$table_vehiculo_imagenes = DB_PREFIJO . 'imagenes';
$field_vehiculo_imagenes = 'url_video';
define('EXIST_FIELD_TABLE_IMAGENES', ( $wpdb->get_var("SELECT column_name FROM information_schema.columns WHERE table_name='$table_vehiculo_imagenes' and column_name='$field_vehiculo_imagenes'") == $field_vehiculo_imagenes));

// Tag del plugin.
add_shortcode('form_busqueda', 'form_busqueda');
add_shortcode('form_busqueda_filter', 'form_busqueda_filter');
add_shortcode('form_busqueda_filter_mobile', 'form_busqueda_filter_mobile');
add_shortcode('form_busqueda_search_mobile', 'form_busqueda_search_mobile');
add_shortcode('form_busqueda_vehiculos_list', 'form_busqueda_vehiculos_list');
add_shortcode('form_busqueda_vehiculo_info', 'form_busqueda_vehiculo_info');
add_shortcode('from_busqueda_vehiculo_button', 'from_busqueda_vehiculo_button');
add_shortcode('form_busqueda_vehiculo_compare', 'form_busqueda_vehiculo_compare');
add_shortcode('form_busqueda_vehiculo_woo', 'form_busqueda_vehiculo_woo');
add_shortcode('form_busqueda_add_woo', 'form_busqueda_add_woo');
