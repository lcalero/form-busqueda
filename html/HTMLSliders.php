<?php

require_once __DIR__. '/../data/DataRangePrecios.php';
require_once __DIR__. '/../data/DataRangeKms.php';
require_once __DIR__. '/../data/DataRangeAnnos.php';


// Plantilla slider.

global $HTML_TEMPLATE_SLIDER;
$HTML_TEMPLATE_SLIDER =
	'<div class="form-group buscadores-rango">
	 <div class="slider-title text-center">{$title}</div>
	 <div class="form-control slider" name="{$name}" unit="{$unit}" min="{$min}" max="{$max}" t-vehiculo="{$tipovehiculo}"></div>
	 <span class="slider-value min float-left" name="{$name}" t-vehiculo="{$tipovehiculo}"></span>
	 <span class="slider-value max float-right" name="{$name}" t-vehiculo="{$tipovehiculo}"></span>
	 </div>';


/**
 * Class FormSlider
 * HTML sliders para precio, km0, antigüedad.
 */
class HTMLSliders {

  private $tipovehiculo = null;


	/**
	 * HTMLSliders constructor.
	 * @param $tipovehiculo Tipo de vehículo.
	 *
	 */
  public function __construct($tipovehiculo) {
    $this->tipovehiculo = $tipovehiculo;
  }



  /**
   * HTML slider precio.
   * @param {object} $mysqli - Conexión DB.
   */
  public function printPrecio( $mysqli ) {
	  global $HTML_TEMPLATE_SLIDER;
	  global $FORM_LOCALES;

    $data = new DataRangePrecios($mysqli);
    $values = $data->getValues($this->tipovehiculo);

    $options = array (
	    'name' => 'precio',
    	'title' => $FORM_LOCALES['precio']['title'],
	    'tipovehiculo' => $this->tipovehiculo,
	    'min' => $values[0],
	    'max' => $values[1],
	    'unit' => $FORM_LOCALES['precio']['unit']
    );

	  printTemplate($HTML_TEMPLATE_SLIDER, $options);
  }



  /**
   * HTML slider km.
   * @param {object} $mysqli - Conexión DB.
   */
  public function printKm( $mysqli ) {
	  global $HTML_TEMPLATE_SLIDER;
	  global $FORM_LOCALES;

    $data = new DataRangeKms($mysqli);
    $values = $data->getValues($this->tipovehiculo);

	  $options = array (
		  'name' => 'km',
		  'title' => $FORM_LOCALES['km']['title'],
		  'tipovehiculo' => $this->tipovehiculo,
		  'min' => $values[0],
		  'max' => $values[1],
		  'unit' => $FORM_LOCALES['km']['unit']
	  );

	  printTemplate($HTML_TEMPLATE_SLIDER, $options);
  }



  /**
   * HTML slider antigüedad.
   * @param {object} $mysqli - Conexión DB.
   */
  public function printAnno( $mysqli ) {
	  global $HTML_TEMPLATE_SLIDER;
	  global $FORM_LOCALES;

    $data = new DataRangeAnnos($mysqli);
    $values = $data->getValues($this->tipovehiculo);

	  $options = array (
		  'name' => 'anno',
		  'title' => $FORM_LOCALES['anno']['title'],
		  'tipovehiculo' => $this->tipovehiculo,
		  'min' => $values[0],
		  'max' => $values[1],
		  'unit' => $FORM_LOCALES['anno']['unit']
	  );

	  printTemplate($HTML_TEMPLATE_SLIDER, $options);
  }
}