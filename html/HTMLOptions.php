<?php

require_once __DIR__. '/../data/DataColor.php';
require_once __DIR__. '/../data/DataMarcas.php';
require_once __DIR__. '/../data/DataModelos.php';
require_once __DIR__. '/../data/DataTransmision.php';
require_once __DIR__. '/../data/DataCombustibles.php';
require_once __DIR__. '/../data/DataUbicacion.php';
require_once __DIR__. '/../data/DataTipologias.php';


/**
 * Class HTMLOptions
 * Devuelve las opciones de marca, modelo, combustible según tipo de búsqueda
 */

class HTMLOptions {

  private $params;



  /**
   * HTMLOptions constructor.
   * @param $tipovehiculo
   */
  public function __construct($params) {
    $this->params = $params;
  }



  /**
   * Output opciones de Marcas.
   * @param {object} $mysqli - Conexión DB.
   */
  public function printAllMarcas( $mysqli, $ma ) {
	  global $FORM_LOCALES;
    $marcas = new DataMarcas($mysqli, $this->params);
    $result = $marcas->getAll();
    $strOptions = $this->getStrOptions($FORM_LOCALES['marcas']['default'], $result, $ma);
    printf($strOptions);
  }



  /**
   *  Output  opciones de Modelos.
   * @param {object} $mysqli - Conexión DB.
   * @param {string} $marca - Identificador de marca.
   */
  public function printModelos( $mysqli, $marca, $modelo) {
    $strOptions = $this->getStrModelos( $mysqli, $marca, $modelo);
    printf($strOptions);
  }



  /**
   * Output opciones de Combustibles.
   * @param {object} $mysqli - Conexión DB.
   */
  public function printAllCombustibles( $mysqli, $combustible ) {
    $strOptions = $this->getStrAllCombustibles( $mysqli, $combustible);
    printf($strOptions);
  }



  /**
   * Devuelve las opciones de Combustibles.
   * @param {object} $mysqli - Conexión DB.
   * @param {string} $marca - Identificador de marca.
   * @param {string} $modelo - Idenntificador modelo.
   */
  public function printCombustibles( $mysqli, $marca, $modelo, $combustible ) {
    $strOptions = $this->getStrCombustibles( $mysqli, $marca, $modelo, $combustible );
    printf($strOptions);
  }

  /**
   * Output opciones de los Colores
   * @param {object} $mysqli - Conexión DB.
   * @param {string} $color - Identificador de color.
   */
  public function  printAllColor( $mysqli ){
      global $FORM_LOCALES;
    $colores = new DataColor( $mysqli, $this->params );
    $result = $colores->getAll();
    $strOptions = $this->getStrOptions($FORM_LOCALES['colores']['default'], $result, null );
    printf($strOptions);
  }

  /**
   * Output opciones de las transmisiones
   * @param {object} $mysqli - Conexión DB.
   * @param {string} $transmision - Identificador de la transmision.
   */
  public function  printAllTransmision( $mysqli ){
    global $FORM_LOCALES;
    $transmision = new DataTransmision( $mysqli, $this->params );
    $result = $transmision->getAll();
    $strOptions = $this->getStrOptions($FORM_LOCALES['transmision']['default'], $result, null );
    printf($strOptions);
  }

  /**
   * Output opciones de las ubicacion
   * @param {object} $mysqli - Conexión DB.
   * @param {string} $ubicacion - Identificador de la ubicacion.
   */
  public function  printAllUbicaciones( $mysqli, $ub ){
    global $FORM_LOCALES;
    $ubicacion = new DataUbicacion( $mysqli, $this->params );
    $result = $ubicacion->getAll();
    $strOptions = $this->getStrOptions($FORM_LOCALES['ubicacion']['default'], $result, $ub );
    printf($strOptions);
  }

  /**
   * Output opciones de las tipologias
   * @param {object} $mysqli - Conexión DB.
   * @param {string} $tipo - Identificador de la tipología.
   */
  public function  printAllTipologias( $mysqli, $tipo ){
    global $FORM_LOCALES;
    $tipologias = new DataTipologias();
    $result = $tipologias->getBy( $mysqli, $this->params['t'] );
    $strOptions = $this->getStrOptions($FORM_LOCALES['tipologia']['default'], $result, $tipo );
    printf($strOptions);
  }

  /**
   *  Devuelve cadena opciones de  Modelos.
   * @param {object} $mysqli - Conexión DB.
   * @param {string} $marca - Identificador de marca.
   * @return {string} Cadena de opciones.
   */
  public function getStrModelos( $mysqli, $marca, $modelo ) {
	  global $FORM_LOCALES;
    $modelos = new DataModelos($mysqli, $this->params);
    $result = $modelos->getBy( $marca );
    $strOptions = $this->getStrOptions($FORM_LOCALES['modelos']['default'], $result, $modelo);

    return $strOptions;

  }



  /**
   * Devuelve las opciones de Combustibles.
   * @param {object} $mysqli - Conexión DB.
   * @param {string} $combustible - Identificador del combustible.
   * @return {string} Cadena de opciones.
   */
  public function getStrAllCombustibles( $mysqli, $combustible ) {
	  global $FORM_LOCALES;
    $combustibles = new DataCombustibles($mysqli, $this->params);
    $result = $combustibles->getAll();
    $strOptions = $this->getStrOptions($FORM_LOCALES['combustibles']['default'], $result, $combustible);

    return $strOptions;
  }



  public function printAllPrecios( $mysqli, $increment, $precio, $ismin ) {
	  global $FORM_LOCALES;
	  $preciosRange = new DataRangePrecios($mysqli);
	  $range = $preciosRange->getValues($this->params);
	  $default = ($ismin == true) ? $FORM_LOCALES['precio']['min'] : $FORM_LOCALES['precio']['max'];
	  $strOptions = $this->getStrOptionsRange($default, $range, $increment, $precio);
	  printf($strOptions);
  }



	public function printAllKms( $mysqli, $increment, $km, $ismin ) {
		global $FORM_LOCALES;
		$kmsRange = new DataRangeKms($mysqli);
		$range = $kmsRange->getValues($this->params);
		$default = ($ismin == true) ? $FORM_LOCALES['km']['min'] : $FORM_LOCALES['km']['max'];
		$strOptions = $this->getStrOptionsRange($default, $range, $increment, $km);
		printf($strOptions);
	}



	public function printAllAnnos( $mysqli, $increment, $anno, $ismin) {
		global $FORM_LOCALES;
		$annosRange = new DataRangeAnnos($mysqli);
		$range = $annosRange->getValues($this->params);
		$default = ($ismin == true) ? $FORM_LOCALES['anno']['min'] : $FORM_LOCALES['anno']['max'];
		$strOptions = $this->getStrOptionsRange($default, $range, $increment, $anno);
		printf($strOptions);
	}



	public function printOrders ($order) {
  	$orders =
			'<option ' . ((empty($order)) ? 'selected="selected"' : '') . ' disabled="disabled" type="">Ordenar por</option>' .
			'<option ' . (($order == 'maknatcode') ? 'selected="selected"' : '') . ' value="maknatcode" type="orderby">Marca</option>' .
			'<option ' . (($order == 'mlocode') ? 'selected="selected"' : '') . ' value="mlocode" type="orderby">Modelo</option>' .
			'<option ' . (($order == 'pvpParticulares') ? 'selected="selected"' : '') . ' value="pvpParticulares" type="orderby">Precio ascendente</option>' .
			'<option ' . (($order == 'pvpParticulares') ? 'selected="selected"' : '') . ' value="pvpParticulares-desc" type="orderbydesc">Precio descendente</option>';
		printf($orders);
	}



  /**
   * Devuelve las opciones de Combustibles.
   * @param {object} $mysqli - Conexión DB.
   * @param {string} $marca - Identificador de marca.
   * @param {string} $modelo - Idenntificador modelo.
   * @return {string} Cadena de opciones.
   */
  public function getStrCombustibles( $mysqli, $marca, $modelo, $combustible ) {
	  global $FORM_LOCALES;
    $combustibles = new DataCombustibles($mysqli, $this->params);
    $result = $combustibles->getBy( $marca, $modelo );
    $strOptions = $this->getStrOptions($FORM_LOCALES['combustibles']['default'], $result, $combustible);

    return $strOptions;
  }


  /**
   * @param $default
   * @param $result
   */
  private function getStrOptions($default, $result, $selectedValue) {
  	$selected = (empty($selectedValue)) ? 'selected="selected"' : '';
    // Opción por defecto.
    $options = '<option ' . $selected . ' disabled="disabled">' . $default . '</option>';

    // Lista de opciones.
    if (!empty($result)) {
      while ($row = mysqli_fetch_assoc($result)) {
      	$value = $row['value'];
      	if (!empty($value)) { // Se eliminan el valor vacío.
		      $selected = (!empty($selectedValue) && $selectedValue == $value) ? 'selected="selected"' : '';
		      $options .= '<option ' . $selected . ' value="' . $value . '">' . $value . '</option>';
	      }
      }
    }

    return $options;
  }


	/**
	 * @param $default
	 * @param $result
	 */
	private function getStrOptionsRange($default, $range, $increment, $value ) {
		$selected = (empty($value)) ? 'selected="selected"' : '';
		$strOptions = '<option  ' . $selected . ' disabled="disabled">' . $default . '</option>';

		$min = intval($range[0]);
		$max = intval($range[1]);

		$interval = $min;
		while ($interval < $max) {
			$selected = (($interval == $value) ? 'selected="selected"' : '');
			$strOptions .= '<option  ' . $selected . ' value="' . $interval . '">' . numberFormat($interval) . '</option>';
			$interval = $interval + $increment;
		}

		if ($interval >= $max) {
			$selected = ($value == $max) ?  'selected="selected"' : '';
			$strOptions .= '<option ' . $selected . ' value="' . $max . '">' . numberFormat($max) . '</option>';
		}

		return $strOptions;
	}
}