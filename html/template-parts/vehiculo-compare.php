<?php
/**
 * Created by IntelliJ IDEA.
 * User: Trabajo
 * Date: 14/01/2020
 * Time: 15:36
 */

$data= $_POST["vehs"];
?>
<div class="row comparador">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <div class="col-sm-12 form-busqueda-part accent-border-color">
      <h3>Compara tus vehículos</h3>
      <span class="uvc-headings-line"></span>
    </div>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
    ?>
    <div class="col-sm-12 col-lg-3 col-md-3">
      <div class="compare-close" data-id="<?php echo $vehiculo['id_vehiculos'] ?>">
        <img src="/wp-content/themes/dt-the7-child/form-busqueda/icons/close-button-compare.png" alt="">
      </div>
      <div class="card box-car <?php echo (empty($vehiculo['id_vehiculoBloqueado'])) ? '' : 'reservado' ?>">
          <div class="card-body">
            <div class="card-img-content">
              <img class="card-img-mini"
                   src="<?php echo (empty($vehiculo["foto"]) || !file_exists('/var/www/vhosts/' . $_SERVER['SERVER_NAME'] . '/httpdocs' . $vehiculo["foto"])) ? get_stylesheet_directory_uri() . '/form-busqueda/icons/Imagen_no_disponible.png' : $vehiculo["foto"] ?>"
                   alt="Card image cap">
            </div>
            <h4 class="card-title"><?php echo $vehiculo["maknatcode"] ?><?php echo $vehiculo["mlocode"] ?></h4>
            <p class="card-text"><?php echo $vehiculo["modnatcode"] . ' ';
              echo $vehiculo['automatico'] == 1 ? $vehiculo["typhp"] : $vehiculo["cv"] ?> Cv</p>
            <div class="card-price">
            <?php if (empty($vehiculo["oferta"])) { ?>
              <div class="col oferta">PVP <?php echo $vehiculo["precio"] ?> €</div>
              <?php } else { ?>
                <div class="oferta">PVP <?php echo $vehiculo["oferta"] ?> €</div>
                <?php } ?>
            </div>
          </div>
      </div>
    </div>
  <?php }
  else{ ?>
    <div class="col-sm-12 col-lg-3 col-md-3">
      <div class="card box-car <?php echo (empty($vehiculo['id_vehiculoBloqueado'])) ? '' : 'reservado' ?>">
        <a href="/vehiculos-ocasion/">
          <div class="card-body">
            <div class="card-img-content">
              <img class="card-img-mini" src="/wp-content/themes/dt-the7-child/form-busqueda/icons/coche.png" alt="Card image cap">
            </div>
            <h4 class="card-title">Añade un <?php echo $user.$pwd ?> para comparar</h4>
          </div>
        </a>
      </div>
    </div>
  <?php }} ?>
</div>
<div class="row compare-listinfo">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td class="compare-value">Combustible</td>
      </tr>
    </table>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
  ?>
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td><?php echo $vehiculo["typtxtfueltypecd2"] ?></td>
      </tr>
    </table>
  </div>
  <?php }
  else{ ?>
  <div class="col-md-3 col-sm-3">
    <table>
      <tr>
        <td class="compare-value">&nbsp;</td>
      </tr>
      </table>
  </div>
  <?php }} ?>
</div>
<div class="row compare-listinfo">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td class="compare-value">Año</td>
      </tr>
    </table>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
      ?>
      <div class="col-sm-12 col-lg-3 col-md-3">
        <table>
          <tr>
            <td><?php echo $vehiculo["yearMatriculacion"] ?></td>
          </tr>
        </table>
      </div>
    <?php }
    else{ ?>
      <div class="col-md-3 col-sm-3">
        <table>
          <tr>
            <td class="compare-value">&nbsp;</td>
          </tr>
        </table>
      </div>
    <?php }} ?>
</div>
<div class="row compare-listinfo">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td class="compare-value">Kilometraje</td>
      </tr>
    </table>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
      ?>
      <div class="col-sm-12 col-lg-3 col-md-3">
        <table>
          <tr>
            <td><?php echo $vehiculo["km"] ?>Km</td>
          </tr>
        </table>
      </div>
    <?php }
    else{ ?>
      <div class="col-md-3 col-sm-3">
        <table>
          <tr>
            <td class="compare-value">&nbsp;</td>
          </tr>
        </table>
      </div>
    <?php }} ?>
</div>
<div class="row compare-listinfo">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td class="compare-value">Potencia(CV)</td>
      </tr>
    </table>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
      ?>
      <div class="col-sm-12 col-lg-3 col-md-3">
        <table>
          <tr>
            <td><?php echo $vehiculo['automatico'] == 1 ? $vehiculo["typhp"] : $vehiculo["cv"] ?> Cv</td>
          </tr>
        </table>
      </div>
    <?php }
    else{ ?>
      <div class="col-md-3 col-sm-3">
        <table>
          <tr>
            <td class="compare-value">&nbsp;</td>
          </tr>
        </table>
      </div>
    <?php }} ?>
</div>
<div class="row compare-listinfo">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td class="compare-value">Transmisión</td>
      </tr>
    </table>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
      ?>
      <div class="col-sm-12 col-lg-3 col-md-3">
        <table>
          <tr>
            <td><?php echo $vehiculo["typtxttranstypecd2"] ?></td>
          </tr>
        </table>
      </div>
    <?php }
    else{ ?>
      <div class="col-md-3 col-sm-3">
        <table>
          <tr>
            <td class="compare-value">&nbsp;</td>
          </tr>
        </table>
      </div>
    <?php }} ?>
</div>
<div class="row compare-listinfo">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td class="compare-value">Nº Puertas</td>
      </tr>
    </table>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
      ?>
      <div class="col-sm-12 col-lg-3 col-md-3">
        <table>
          <tr>
            <td><?php echo $vehiculo["typdor"] ?></td>
          </tr>
        </table>
      </div>
    <?php }
    else{ ?>
      <div class="col-md-3 col-sm-3">
        <table>
          <tr>
            <td class="compare-value">&nbsp;</td>
          </tr>
        </table>
      </div>
    <?php }} ?>
</div>
<div class="row compare-listinfo">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td class="compare-value">Color exterior</td>
      </tr>
    </table>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
      ?>
      <div class="col-sm-12 col-lg-3 col-md-3">
        <table>
          <tr>
            <td><?php echo $vehiculo["color"] ?></td>
          </tr>
        </table>
      </div>
    <?php }
    else{ ?>
      <div class="col-md-3 col-sm-3">
        <table>
          <tr>
            <td class="compare-value">&nbsp;</td>
          </tr>
        </table>
      </div>
    <?php }} ?>
</div>
<div class="row compare-listinfo">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td class="compare-value">Consumo</td>
      </tr>
    </table>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
      ?>
      <div class="col-sm-12 col-lg-3 col-md-3">
        <table>
          <tr>
            <td><?php echo $vehiculo["mediaconsumo"] ?> l/100</td>
          </tr>
        </table>
      </div>
    <?php }
    else{ ?>
      <div class="col-md-3 col-sm-3">
        <table>
          <tr>
            <td class="compare-value">&nbsp;</td>
          </tr>
        </table>
      </div>
    <?php }} ?>
</div>
<div class="row compare-listinfo">
  <div class="col-sm-12 col-lg-3 col-md-3">
    <table>
      <tr>
        <td class="compare-value">Emisiones</td>
      </tr>
    </table>
  </div>
  <?php for ( $i = 0; $i < 3; $i++ ) {
    if(!empty($data[$i])) {
      $dataVehiculo = new DataVehiculo($data[$i]);
      $vehiculo = $dataVehiculo->get();
      ?>
      <div class="col-sm-12 col-lg-3 col-md-3">
        <table>
          <tr>
            <td><?php echo $vehiculo["tcoco2emi"] ?> gr/km (CO2)</td>
          </tr>
        </table>
      </div>
    <?php }
    else{ ?>
      <div class="col-md-3 col-sm-3">
        <table>
          <tr>
            <td class="compare-value">&nbsp;</td>
          </tr>
        </table>
      </div>
    <?php }} ?>
</div>

