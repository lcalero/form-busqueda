<script>
  (function ($) {
    $(document).ready(function () {
        $('#billing_marca').val('<?php echo $marca ?>');
        $('#billing_modelo').val('<?php echo $modelo ?>');
        $('#billing_version').val('<?php echo $version ?>');
        $('#billing_url').val('<?php echo $url ?>');
    });
  })(jQuery);
</script>
