<!-- LISTA DE VEHICULOS -->
<div class="row listado" t-vehiculo="<?php echo $tipovehiculo ?>">

  <?php
  foreach ( $data as $row ) {
	  $row = parseDataVehiculo($row);
	?>

  <!-- VEHICULO -->
  <div class="col-sm-4">
    <div class="card">
      <a href="<?php echo $row["ficha"] ?>">
      <img class="card-img-top" src="<?php echo $row["foto"] ?>" alt="Card image cap">
      <div class="card-body">
        <h5 class="card-title"><?php echo $row["maknatcode"] ?> <?php echo $row["mlocode"] ?></h5>
        <p class="card-text"><?php echo $row["modnatcode"] ?> <?php echo $row["cv"] ?> Cv</p>
        <div class="row">
          <div class="col anno"><?php echo $row["yearMatriculacion"] ?></div>
          <div class="col km"><?php echo $row["km"] ?> kms</div>
        </div>
        <div class="row">
          <div class="col fueltype"><?php echo $row["typtxtfueltypecd2"] ?></div>
          <div class="col precio"><?php echo $row["precio"] ?> €</div>
        </div>
      </div>
      </a>
    </div>
  </div>

  <?php } ?>
</div>