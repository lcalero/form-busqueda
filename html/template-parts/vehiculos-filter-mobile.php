<div class="form-group col-md-12">
  <button class="btn btn-show-form-busqueda-mobile" t-vehiculo="<?php echo $tipovehiculo ?>">
    <span><i class="fa fa-filter"></i> Filtrar</span>
  </button>
</div>

<div class="form-busqueda-mobile container-fluid hidden" t-vehiculo="<?php echo $tipovehiculo ?>">

  <div class="form-container container">
    <div class="row form-busqueda-header">
      <!--boton cruz para ocultar -->
      <div class="form-group col-md-12">
        <button class="btn btn-hide-form-busqueda-mobile" t-vehiculo="<?php echo $tipovehiculo ?>">X</button>
      </div>
    </div>

    <div class="row form-container-inside">
      <div class="form-group col-sm-12 bottom-30 form-busqueda-part ma">
        <p class="text-center">¿Qué marcas buscas?</p>
        <select class="form-control marca mobile" name="ma" t-vehiculo="<?php echo $tipovehiculo ?>">
          <?php
          $formOptions->printAllMarcas($mysqli, $ma);
          ?>
        </select>
      </div>

      <div class="form-group col-sm-12 bottom-30 form-busqueda-part mo">
        <p class="text-center">¿Buscas un modelo específico?</p>
        <select class="form-control modelo mobile" name="mo" t-vehiculo="<?php echo $tipovehiculo ?>">
          <?php
          if (!empty($ma)) {
            $formOptions->printModelos($mysqli, $ma, $mo);
          }
          else {
            printf('<option disabled="disabled" selected="selected">Modelo</option>');
          }
          ?>
        </select>
      </div>

      <div class="form-group col-sm-12 bottom-30 form-busqueda-part co">
        <p class="text-center">¿Qué combustible deseas?</p>
        <select class="form-control combustible mobile" name="co" t-vehiculo="<?php echo $tipovehiculo ?>">
          <?php
          if (!empty($ma)) {
            $formOptions->printCombustibles($mysqli, $ma, $mo, $co);
          }
          else {
            $formOptions->printAllCombustibles($mysqli);
          }
          ?>
        </select>
      </div>

      <div class="form-group col-sm-12 bottom-0 form-busqueda-part">
        <p class="text-center">TU PRESUPUESTO</p>
      </div>

      <div class="form-group col-sm-12 form-busqueda-part">
        <select class="form-control precio mobile" name="minprecio" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
          <?php $formOptions->printAllPrecios($mysqli, 2000,  $minprecio, true); ?>
        </select>
      </div>

      <div class="form-group col-sm-12 form-busqueda-part">
        <select class="form-control precio mobile" name="maxprecio" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
          <?php $formOptions->printAllPrecios($mysqli, 2000, $maxprecio, false); ?>
        </select>
      </div>

      <!--
      <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
        <select class="form-control km mobile" name="minkm" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
          <?php $formOptions->printAllKms($mysqli, 10000, $minkm, true); ?>
        </select>
      </div>

      <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
        <select class="form-control km mobile" name="maxkm" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
          <?php $formOptions->printAllKms($mysqli, 10000, $maxkm, false); ?>
        </select>
      </div>

      <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
        <select class="form-control anno mobile" name="minanno" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
          <?php $formOptions->printAllAnnos($mysqli, 1, $minanno, true); ?>
        </select>
      </div>

      <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
        <select class="form-control año mobile" name="maxanno" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
          <?php $formOptions->printAllAnnos($mysqli, 1, $maxanno, false); ?>
        </select>
      </div>


      <div class="form-group col-md-12">
        <select class="form-control order mobile" name="order" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
          <?php $formOptions->printOrders($order); ?>
        </select>
        <input type="hidden" name="orderby" t-vehiculo="<?php echo $tipovehiculo ?>">
        <input type="hidden" name="orderbydesc" t-vehiculo="<?php echo $tipovehiculo ?>">
      </div>

      -->

    </div>
    <div class="row form-busqueda-footer">
      <div class="form-group col-sm-12 text-center">
        <button class="btn reset" t-vehiculo="<?php echo $tipovehiculo ?>">Ver todos</button>
      </div>
    </div>
  </div>



</div>