<?php
/**
 * Created by IntelliJ IDEA.
 * User: Huawei
 * Date: 04/06/2018
 * Time: 21:42
 */

// HTML not result.
global $HTML_TEMPLATE_NOT_RESULT;
$HTML_TEMPLATE_NOT_RESULT =
	'<div class="row vehiculos">
		<div class="col-md-12">
			<h4>{$msg}</h4>
		</div>
	</div>';


// Consulta los vehículos.
require_once __DIR__ . '/../data/DataVehiculos.php';


class HTMLVehiculos {


	/**
	 * Muestra los vehículos según
	 *
	 * @param $mysqli
	 * @param $params
	 */
	public function printBy( $mysqli, $params ) {
		global $HTML_TEMPLATE_NOT_RESULT;
		global $FORM_LOCALES;

		$dataVehiculos = new DataVehiculos( $mysqli );
		$tipo_vehiculos = $params['t'];
		$data          = $dataVehiculos->getBy( $params );
		$numcolsclass  = $params['numcolsclass'];

        $result = $this->getResult($params, $data);
		// Sin resultados en la búsqueda.
		if ( $result['total'] == 0 ) {
			$options = array(
				'msg' => $FORM_LOCALES['not-result']
			);
			printTemplate( $HTML_TEMPLATE_NOT_RESULT, $options );
		} else {
			$this->printResult( $tipo_vehiculos, $result['result'], $numcolsclass, $params );
            if ( ($params['showpagination'] === 'true' || $params['showpagination'] === true) && $params['start'] != 'ALL') {
              $this->printPaginacion($params['pag'], $result['total']);
            }
		}
	}

	public function getResult ($params, $data){
        $data2 = array();
        if (!empty($params['buscar'])){
          $palabras = explode(" ", $params['buscar']);
          while($fila = mysqli_fetch_assoc($data)){
            foreach ($palabras as $palabra) {
              $pos = strpos(strtolower($fila['busqueda']), strtolower($palabra), 0);
              if($pos !== false){
                $data2[] = $fila;
              }
            }
          }
        }else{
          while ( $row = mysqli_fetch_assoc( $data ) ) {
            $data2[] = $row;
          }
        }

        $start = $params['start'];
        if (!empty($start) && $params['showpagination'] !== 'true' && $start != 'ALL') {
          $dataTotal = $start;
        }else{
          $dataTotal = count($data2);
        }

        $pag = $params['pag'];
        $start = $params['start'];
        if (!empty($pag)) {
          $data2 = array_slice($data2, $pag - 1, VEHICULOS_POR_PAGINA);
        }
        // resultados sin paginación con límite.
        else {
          if (!empty($start)) {
            if ($start != 'ALL') {
              $data2 = array_slice($data2, 0, $start);
            }
          }
          else {
            $data2 =  array_slice($data2, 0, VEHICULOS_POR_PAGINA);
          }
        }
      return [
        'total' => $dataTotal,
        'result' => $data2
      ];
    }

	/**
	 * @param $result
	 */
	private function printResult( $tipovehiculo, $result, $numcolsclass, $params ) {

	  $data = $result;
		if (TEMPLATE_VEHICULOS_LIST) {
			include get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/vehiculos-list.php';
		}
		else {
			include 'template-parts/vehiculos-list.php';
		}

	}


	/**
	 * @param $pag
	 * @param $total
	 *
	 */
	private function printPaginacion( $pag, $total ) {

		if ( empty( $pag ) ) {
			$pag = 1;
		}

		// Botones de paginación.
		$totalPages = ceil( $total / VEHICULOS_POR_PAGINA );
		if ( $totalPages > 1 ) {
			$start = 1;
			if (TEMPLATE_VEHICULOS_PAGINATION) {
				include get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/vehiculos-pagination.php';
			}
			else {
				include 'template-parts/vehiculos-pagination.php';
			}

		}
	}
}