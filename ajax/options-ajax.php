<?php

  function optionsAjax() {

    // Parámetros.
    @$params = ['t'=> strtolower($_GET['t'])];
    @$marca = strtolower($_GET['ma']);
    @$modelo = strtolower($_GET['mo']);


    // DB connection.
	  $mysqli = conectionDB();
    if ($mysqli->connect_error) {
      die("Connection failed: " . $mysqli->connect_error);
    }


    // Lista de modelos según marca, y todos los combustibles.
    if (!empty($params) && empty($marca) && empty($modelo)){
      $formOptions = new HTMLOptions($params);
      $str_cumbustibles = $formOptions->getStrAllCombustibles($mysqli, null);
      echo json_encode([
        'modelos' => '<option disabled="disabled" selected="selected">Modelo</option>', //TODO Hay ponerlo por locales por AJAX
        'combustibles' =>$str_cumbustibles
      ]);
    }


    // Lista de modelos según marca, y todos los combustibles.
    else if (!empty($params) && !empty($marca) && empty($modelo)){
      $formOptions = new HTMLOptions($params);
      $str_modelos = $formOptions->getStrModelos($mysqli, $marca, null);
      $str_cumbustibles = $formOptions->getStrAllCombustibles($mysqli, null);
      echo json_encode([
        'modelos' =>$str_modelos,
        'combustibles' =>$str_cumbustibles
      ]);
    }


    // Lista de combustibles según marca y modelo..
    else if (!empty($params) && !empty($marca) && !empty($modelo)){
      $formOptions = new HTMLOptions($params);
      $str_cumbustibles = $formOptions->getStrCombustibles($mysqli, $marca, $modelo, null);
      echo json_encode([
        'combustibles' =>$str_cumbustibles
      ]);
    }

    else {
      echo ('La opción no existe...');
    }

    // DB close.
	  closeDB($mysqli);

    wp_die();
  }

  add_action('wp_ajax_nopriv_options_ajax', 'optionsAjax' );
  add_action('wp_ajax_options_ajax', 'optionsAjax' );