<?php
// =============================================================================
// TEMPLATE NAME:Template para Búsqueda de Coches
// -----------------------------------------------------------------------------
// =============================================================================
?>

<?php

/**
 *
 */
function ajaxBusqueda() {
	//TODO Google analytic
	$mysqli = conectionDB();
	$busqueda = new HTMLVehiculos();
    $dataVehiculos = new DataVehiculos( $mysqli );

    ob_start(); //Activa el almacenamiento en búfer de la salida
    $busqueda->printBy($mysqli, $_GET);
    $resultado = ob_get_clean(); //Obtiene el contenido del búfer actual y elimina el búfer de salida actual

    $data = $dataVehiculos->getBy( $_GET );
	$total = $busqueda->getResult($_GET, $data) ;

    echo json_encode([
      'resultado' => $resultado,
      'total' => $total['total']
    ]);

	closeDB($mysqli);

	wp_die();
}

add_action('wp_ajax_nopriv_busqueda_ajax', 'ajaxBusqueda' );
add_action('wp_ajax_busqueda_ajax', 'ajaxBusqueda' );
