<?php

define('PLUGIN_NAME', 'form-busqueda');

// PATH donde se encuanta este plugin.
define('PLUGIN_PATH', dirname(__FILE__));

define('PLUGIN_LANG', 'es');
define('PLUGIN_LANG_EXTEND', false);

// Prejijo BBDD

define('DB_PREFIJO', $GLOBALS['table_prefix']);

// TIPOS VEHICULOS

define('TIPO_VEHICULO_NUEVO', 'vn');
define('TIPO_VEHICULO_NUEVO_OFERTA', 'vno');
define('TIPO_VEHICULO_OCASION', 'vo');
define('TIPO_VEHICULO_OCASION_MAS_KM0', 'vokm0');
define('TIPO_VEHICULO_OCASION_OFERTA', 'voo');
define('TIPO_VEHICULO_KM0', 'km0');
define('TIPO_VEHICULO_OCASION_NO_FURGON', 'vonf');
define('TIPO_VEHICULO_OCASION_FURGON', 'vof');
define('TIPO_VEHICULO_DEMO', 'demo');
define('TIPO_VEHICULO_SUBASTA', 'vokm0s');
define('TIPO_VEHICULO_OCASION_NO_SUBASTA', 'vokm0ns');
define('TIPO_VEHICULO_KM0_NO_SUBASTA', 'km0ns');
define('TIPO_VEHICULO_ECO', 'eco');

// Busqueda con oferta de particulares.
define('PVP_OFERTA_PARITCULARES', false);

// DEFINE EL NOMBRE DEL CAMPO PARA LA BASE DE DATOS WORDPRESS DONDE GUARDAMOS LAS OPCIONES
define('OPCIONES_CONFIGURACION', 'fb-options');
define('BUSCADOR_NUEVO', 'fb-new');
// Obtener configuración de la base de datos en el caso de que existan
$opciones = get_option( OPCIONES_CONFIGURACION );

// Precio mínimo de búsqueda.
define('FIELD_VEHICULOS_PRECIO_MINIMO', 'precio_minimo');
define('VEHICULOS_PRECIO_MINIMO_DEFAULT', 2900);
define('VEHICULOS_PRECIO_MINIMO', (isset($opciones[FIELD_VEHICULOS_PRECIO_MINIMO]) ? $opciones[FIELD_VEHICULOS_PRECIO_MINIMO] : VEHICULOS_PRECIO_MINIMO_DEFAULT));

// Cantidad de vehículos en la carga inicial de la página (null por defecto).

define('VEHICULOS_CANTIDAD_INICIAL', 8);

// Cantidad de vehículos por página que se muestran.
define('FIELD_VEHICULOS_POR_PAGINA', 'vehiculo_pagina');
define('VEHICULOS_POR_PAGINA_DEFAULT', 8);
define('VEHICULOS_POR_PAGINA', (isset($opciones[FIELD_VEHICULOS_POR_PAGINA]) ? $opciones[FIELD_VEHICULOS_POR_PAGINA] : VEHICULOS_POR_PAGINA_DEFAULT));


// Path para mostrar la información de un vehículo.

define('VEHICULO_INFO_PATH', '?{$maknatcode}-{$mlocode}-{$typtxtfueltypecd2}-{$id_vehiculos}');

// Path pa mostrar la página de financiación.
define('VEHICULO_FINANCIACION_PATH', 'http://auto.bbvaconsumerfinance.es/#!/home?msi=01828582047300000000226127000000000&m-r={$tarifa}&g-i=00001&s-i=00001&b-i={$codMarca}&b-d={$marca-finan}%20%20&m={$modelo-finan}%20&v={$modnatcode}&p={$pvpParticulares}&p-id={$codProducto}&d-d=Altamar&d-c=40.416778,-3.703282{$tipoURL}&m-s=01828582047500000000298782000000000&d=0');

// PLANTILLA dentro del tema para el filtro de vehículos.
define('TEMPLATE_VEHICULOS_FILTER', true);

// PLANTILLA dentro del tema para mostrar la lista de vehículos

define('TEMPLATE_VEHICULOS_LIST', true);

// PLANTILLA dentro del tema para mostrar la paginación

define('TEMPLATE_VEHICULOS_PAGINATION', false);

// PLANTILLA dentro del tema para mostrar la información del vehículo

define('TEMPLATE_VEHICULO_INFO', true);

define('TEMPLATE_VEHICULO_WOO', false);

define('CSS_VEHICULOS_LIST', true);


define('CSS_VEHICULOS_INFO', true);

//Varibles financiación producto tipo de vehiculo
define('FINANCIACION_COD_PRODUCTO_VN', '0GV0002451');
define('FINANCIACION_COD_PRODUCTO_VO_MENOR_5_ANNOS', '0000008511');
define('FINANCIACION_COD_PRODUCTO_VO_MAYOR_5_ANNOS', '0GV0002453');

define('FINANCIACION_COD_TARIFA_NUEVO_SEMINUEVO', '01828582047400000000942083000000000');
define('FINANCIACION_COD_TARIFA_USADO', '01828582047400000000942084000000000');

//Variable para indicar el nº del tipo en los Equipamientos del vehiculo
define('FIELD_EQUIPAMIENTO_SERIE', 'equipamiento_serie');
define('EQUIPAMIENTO_SERIE_DEFAULT', '0');
define('EQUIPAMIENTO_SERIE', (isset($opciones[FIELD_EQUIPAMIENTO_SERIE]) ? $opciones[FIELD_EQUIPAMIENTO_SERIE] : EQUIPAMIENTO_SERIE_DEFAULT));

