<?php


/**
 * Author: Juan Carlos Muruaga Ceballos
 */

global $FORM_LOCALES;
$FORM_LOCALES = array(
	'marcas'=>[
		'title'=>'¿Qué marcas buscas?',
		'default'=>'Marca'
	],
	'modelos'=>[
		'title'=>'¿Buscas un modelo específico?',
		'default'=>'Modelo'
	],
	'combustibles'=>[
		'title'=>'¿Qué combustible deseas?',
		'default'=>'Combustible'
	],
	'precio'=>[
		'title'=>'Precio entre',
		'unit'=>'€',
		'min'=>'Precio mínimo',
		'max'=>'Precio máximo'
	],
	'km'=>[
		'title'=>'Kilómetros',
		'unit'=>'kms',
		'min'=>'Kilómetros mínimo',
		'max'=>'Kilómetros máximo'
	],
	'anno'=>[
		'title'=>'Antigüedad',
		'unit'=>'año(s)',
		'min'=>'Años mínimo',
		'max'=>'Años máximo'
	],
    'colores'=>[
      'title'=>'¿Qué color buscas?',
      'default'=>'Color'
    ],
    'transmision'=>[
      'title'=>'¿Qué transmision buscas?',
      'default'=>'Transmisión'
    ],
    'ubicacion'=>[
      'title'=>'¿Que ubicación deseas?',
      'default'=>'Ubicación'
    ],
    'tipologia'=>[
      'title'=>'¿Que tipo buscas?',
      'default'=>'Tipologías'
    ],
	'not-result' => 'No existe ningún resultado para esta búsqueda. Inténtelo con otros valores.',
	'clean' => 'Limpiar',
	'pagination-before' => 'Anterior',
	'pagination-next' => 'Siguiente'
);