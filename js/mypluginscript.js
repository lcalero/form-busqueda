(function ($) {
  $(document).ready(function () {

    $('ul.nav-tabs > li').on('click', function (e) {
      var tab_id = $(this).find('a').attr('href');

      $('ul.nav-tabs li.active').removeClass("active");
      $('.tab-pane.active').removeClass("active");
      e.preventDefault();

      $(this).addClass('active');
      $(tab_id).addClass('active');
    });

  });
})(jQuery);